# Feature XXXXXXX

* Responsible person: Marko Polo
* Status (Accepted/Postponed/Rejected etc)

### Description

Sed non nisi id ligula interdum mattis. Fusce vel ullamcorper nunc. Nulla pharetra dui ut enim semper semper. Aenean ut leo tortor. Fusce felis nibh, malesuada vitae nibh at, rhoncus feugiat leo. Nam vehicula vitae ligula vitae condimentum. Ut maximus metus nec lorem ultrices elementum.


### Affected Use Cases (Use Cases)

Linked Use Cases

* [Use Case 1](FT1-kayttotapaus.md)
* [Use Case 2](FT2-kayttotapaus.md)


### User Storys (User Storys)

* _As a user I want to see Lorem Ipsum on the site, because it makes me feel safe._
* _As an administrator I want the greeting to change every now and then, because it is refreshing!_


Link to real User Storys

- [ ] #2
- [ ] #6
- [ ] #5






### User Interface Mockup/Design

![](https://openclipart.org/image/300px/svg_to_png/178764/1370010418.png&disposition=attachment)


### Functional Requirements

**For example**

* User sees the "Lorem Ipsum" message on screen at all times
* Administrator can edit the welcome-message


### Non-Functional Requirements


* Usability: Generating the new welcome message takes < 0.5 s
* Security:
* Scalability:
* 


### Restrictions 

* Al
* 


### Test Cases for Feature

| Test Case  | Source for test  | Who is responsible  |
|:-: | :-:|:-:|
| [Test Case 1]( FT1-testitapaus1.md)  | Use Case 1  |  |
| [Test Case 2]( FT1-testitapaus2.md)  |  |  |
| [Test Case 3]( FT1-testitapaus3.md)  | Use Case 2 |  |
| [Test Case 4]( FT1-testitapaus4.md)  | Requirement REQ001 |  |
| [Testitapaus 5]( FT1-testitapaus5.md)  |  |  |
| [Testitapaus 6]( FT1-testitapaus6.md)  |  |  |


### Schedule/Roadmap

| Status | |
|:----:|:----:|
| Accepted/ Rejected | 1.1.2014 |

/label ~Feature


