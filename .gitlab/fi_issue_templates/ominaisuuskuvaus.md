# Ominaisuus XXXXXXX

* Vastuuhenkilö: Marko Polo
* Status (Hyväksytty/Hylätty/Lykätty etc)

### Kuvaus

Sed non nisi id ligula interdum mattis. Fusce vel ullamcorper nunc. Nulla pharetra dui ut enim semper semper. Aenean ut leo tortor. Fusce felis nibh, malesuada vitae nibh at, rhoncus feugiat leo. Nam vehicula vitae ligula vitae condimentum. Ut maximus metus nec lorem ultrices elementum.


### Liittyvät käyttötapaukset (Use Cases)

Liittyy seuraaviin käyttötapauksiin:

* [Use Case 1](FT1-kayttotapaus.md)
* [Use Case 2](FT2-kayttotapaus.md)


### Käyttäjätarinat (User Storys)

* _Käyttäjänä haluan, että lorem ipsum on näkyvillä koko käytön ajan, koska se tuo minulle turvallisen olon._
* _Ylläpitäjänä haluan, että tervehdysviesti on vaihdettavissa, koska se virkistää työntekoa!_



Linkit käyttäjätapauksiin

- [ ] #2
- [ ] #6
- [ ] #5






### Käyttöliittymä Mockup/Design

![](https://openclipart.org/image/300px/svg_to_png/178764/1370010418.png&disposition=attachment)


### Toiminnalliset vaatimukset

**Esimerkiksi**

* Käyttäjä näkee jatkuvasti ruudulla "Lorem Ipsum"-viestin
* Tervehdysviesti on pääkäyttäjän muokattavissa


### Ei-tominnalliset vaatimukset


* Käytettävyys: Uuden tervehdysviestin generointi saa kestää < 0.5 s
* Turvallisuus:
* Skaalautuvuus:
* 


### Rajoitukset 

* Al
* 


### Ominaisuuden testitapaukset

| Testitapaus  | Testin lähde  | Vastuuhenkilö  |
|:-: | :-:|:-:|
| [Testitapaus 1]( FT1-testitapaus1.md)  | Use Case 1  | Marko Polo |
| [Testitapaus 2]( FT1-testitapaus2.md)  |  |  |
| [Testitapaus 3]( FT1-testitapaus3.md)  | Use Case 2 |  |
| [Testitapaus 4]( FT1-testitapaus4.md)  | Requirement REQ001 |  |
| [Testitapaus 5]( FT1-testitapaus5.md)  |  |  |
| [Testitapaus 6]( FT1-testitapaus6.md)  |  |  |


### Aikataulu

| Status | |
|:----:|:----:|
| Hyväksytty/Hylätty | 1.1.2014 |

/label ~Feature
