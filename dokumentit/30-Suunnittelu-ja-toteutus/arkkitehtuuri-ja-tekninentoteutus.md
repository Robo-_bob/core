# Arkkitehtuuri ja tekniset ratkaisut

## WimmaLab forum

0.1

Julkinen dokumentti

Vastuuhenkilö
Riku Härkälä

P1384@student.jamk.fi



## Johdatus

Tässä dokumentissa kuvataan yleisellä tasolla miten Corpo Ration Corp (JAMK TTK20S1OTL/te-team1) on kehittänyt WimmaLabia varten 
keskustelufoorumia, joka perustuu avoimeen "Conduit"-palveluun; [Github gothinkster](https://github.com/gothinkster/realworld).



## Yleiskuvaus

* Keskustelufoorumi, joka on tarkoitettu WimmaLabiin osallistuville.
* Tuote on toteutettu projektityönä perustuen gothinksterin lähdekoodeihin.
* Tuotteen jatkokehitystä varten tarvitaan Docker-kontitusympäristö sekä React ja NodeJS -ohjelmointikieliin sopiva koodieditori.


## Käytetyt teknologiat

  * Frontend: React
  * Backend: NodeJS/Express
  * Tietokanta: MongoDB
  * koodaus: npm, html, css, JavaScript

  
## Tuotekehitysympäristöjen kuvaukset

* Kehitysympäristö: Windows 10 / Linux Mint / Ubuntu
* Testausympäristö: virtualisoitu Linux, Virtualbox
* Ajo/suoritusympäristö: Linux ja Docker
* Demoympäristö: 2 x Linux-palvelin (Ubuntu server 20.04) toteutettuna CSC:n Pouta-pilvipalvelussa (Open stack)


## Käytetyt työvälineet

* Visual Studio Code
* debuggeri zky v2.05
* Notepad ++
* Sublime Text Version
* gedit
* GNU nano
* Brackets
* Git for Windows
* Gitlab
* xed
* Firefox
* Chrome
* Brave Version 1.17.72 Chromium
* Microsoft Excel
* Microsoft PowerPoint
* Microsoft Teams
* LibreOffice Draw
* Ghostwriter




## Tärkeimmät tekniset ratkaisut joihin tuote nojaa


  * Käytetyt kehikot: React, NodeJS
  * Kyseiset tekniikat oli product owner (Marko Rintamäki) valikoinut gothinksterin esimerkkivalikoimasta
  

## Suoritysympäristön (tuotanto) kuvaus

  * Ubuntu server 20.04, virtualisoituna CSC:n Pouta-pilvipalvelussa 
      * Docker-kontitusympäristö, 
      * Apache2 http-palvelin: reverse proxy, SSL-sertifiointi ja liikenteen ohjaus https-protokollan kautta internetin (käyttäjien) suuntaan
  * Backup server Ubuntu server 20.04, virtualisoituna CSC:n Pouta-pilvipalvelussa
      * Ajastetut varmuuskopiot tietokannasta ja palvelun käyttölogeista SSH-yhteyden kautta

```plantuml
@startuml
actor User

cloud "Internet" as net{
queue "https"{
}
}

node "CSC srv1 / Ubuntu" as csc {
queue http {
}
node Docker {
node "Frontend" {
}
node "Backend" {
}
database "Mongo" {
}
}
card "Reverse Proxy / Apache" as rpa {
}
}
queue SSH {
}
node "CSC srv2 / Ubuntu" as csc2 {
database "database backup" as dbb {
}
database "sercive logs backup" as slb {
}
node "Backend" {
}
database "Mongo" {
}
}
User -- https
https -- rpa
rpa -- http
rpa -- http
Backend -- Mongo
http -- Backend
http -- Frontend
csc -- SSH
SSH -- slb
SSH -- dbb
@enduml
```

  * ![Sijoittelunäkymä](topo1.png)


## Tietokantakuvaukset


* MongoDB
* Varmuuskopiot tietokannasta automatisoitu toiselle palvelimelle, toteutettu python-ohjelmointikielellä: python käyttää omaa ajastintaan
ja ajaa varmuuskopiointi-scriptit.





