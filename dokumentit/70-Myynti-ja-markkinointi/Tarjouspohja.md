# Tarjouspohja
<!---
Alkuperäisen pohjan laatijat: 

Elina Monthan, Tommi P, Aleksi L, Jarkko U ja Joonas V
-->  


# Tarjouksen vastaanottaja:	  Wimma Lab

Viitaten tarjouspyyntöönne 14.9.2020 tarjoamme teille seuraavaa palvelutuotetta: 

## 1. Tarjouksen kohde ja hankintatilanne

Totetutettvalla "WimmaForum" – palvelulla tarkoitetaan tässä tapauksessa verkkopalvelua, joka käsittää seuraavat toiminnallisuudet:

| Ominaisuus | Prioriteetti | Use cases |
|:-|:-:|:-:|
| [Feature 1 - GDPR](http://te-team1.pages.labranet.jamk.fi/core/20-Vaatimustenhallinta/Featuret/feature01-gdpr.md) | P1 | [GDPR](http://te-team1.pages.labranet.jamk.fi/core/20-Vaatimustenhallinta/use_cases/gdpr_hyvaksyminen.md) |
| [Feature 2 - salasananpalautus](http://te-team1.pages.labranet.jamk.fi/core/20-Vaatimustenhallinta/Featuret/feature02-salasananpalautus.md) | P2 |  [salasananpalautus](http://te-team1.pages.labranet.jamk.fi/core/20-Vaatimustenhallinta/use_cases/salasanan_palautus.md) |
| [Feature 3 - blacklist](http://te-team1.pages.labranet.jamk.fi/core/20-Vaatimustenhallinta/Featuret/feature03-blocklist.md) | P3 | [blocklist](use_cases/kiellettyjen_sanojen_estäminen.md) |
| [Feature 4 - moderointipalvelu.md](http://te-team1.pages.labranet.jamk.fi/core/20-Vaatimustenhallinta/Featuret/feature04-moderointipalvelu.md) | P1 | [moderointi](http://te-team1.pages.labranet.jamk.fi/core/20-Vaatimustenhallinta/use_cases/moderointi_viestin_poisto.md) |
| [Feature 5 - käyttäjäroolit](http://te-team1.pages.labranet.jamk.fi/core/20-Vaatimustenhallinta/Featuret/feature05-kayttajaroolit.md) | P1 | [käyttäjäroolit](http://te-team1.pages.labranet.jamk.fi/core/20-Vaatimustenhallinta/use_cases/antaa_moderointi_oikeudet.md) |
| [Feature 6 - palaute](http://te-team1.pages.labranet.jamk.fi/core/20-Vaatimustenhallinta/Featuret/feature06-palaute.md) | P2 | [palaute](use_cases/palautteen_anto.md) |
| [Feature 7 - backendlog](http://te-team1.pages.labranet.jamk.fi/core/20-Vaatimustenhallinta/Featuret/feature07-backendlog.md) | P2 | [backendlog](use_cases/backend_log_katsominen.md) |
| [Feature 8 - googleanalytics](http://te-team1.pages.labranet.jamk.fi/core/20-Vaatimustenhallinta/Featuret/feature08-googleanalytics.md) | P3 |  |
| [Feature 9 - Docker](http://te-team1.pages.labranet.jamk.fi/core/20-Vaatimustenhallinta/Featuret/feature09-Docker.md) | P1 | |
| [Feature 10 - HTTPS](http://te-team1.pages.labranet.jamk.fi/core/20-Vaatimustenhallinta/Featuret/feature10-TLS.md) | P1 |  |
| [Feature 11 - Branding](http://te-team1.pages.labranet.jamk.fi/core/20-Vaatimustenhallinta/Featuret/feature11-branding.md) | P1 |  |
| [Feature 12 - Some](http://te-team1.pages.labranet.jamk.fi/core/20-Vaatimustenhallinta/Featuret/feature12-some.md) | P1 | [sosiaaliseen mediaan jakaminen](use_cases/sosiaaliseen_mediaan_jakaminen.md) |

**Tarkemmat ominaisuuskuvauset löytyvät [vaatimusmäärittelyssä]()**


## 2. Tavoitteet, vaatimukset ja ehdotetut ratkaisut

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Fproto%2FuISbBcn9myJVnNOmBtkb6h%2FWimmaLab-Forum%3Fnode-id%3D3%253A4%26viewport%3D735%252C944%252C0.6867609620094299%26scaling%3Dscale-down" allowfullscreen></iframe>

## 3. Toimitussuunnitelma

⦁	Vaiheistus ja aikataulu  (liite 2)

###  Etapit:

* [Sprint 00 14.9.2020-20.9.2020](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/2) Käynnistys
* [Sprint 01 21.9.2020-27.9.2020](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/3) 
* [Sprint 02 28.9.-4.10.2020](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/4) 
* [Sprint 03 5.10.-11.10.2020](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/5) 
* [Sprint 04 12.10-18.10.2020](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/6)
* [Sprint 05 19.10-25.10.2020](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/14)
* [Sprint 06 26.10-1.11.2020](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/7)
* [Sprint 07 2.11-8.11.2020](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/8)
* [Sprint 08 9.11-15.11.2020](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/9)
* [Sprint 09 16.11-22.11.2020](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/10)
* [Sprint 10 23.11-29.11.2020](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/11)
* [Sprint 11 30.11-4.12.2020](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/15)
* [Sprint End 12.12-13.12.2020](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/12)

Lopetus 12.12.2020 – 13.12.2020 (14 h)

* [Yleistestaussuunnitelma](http://te-team1.pages.labranet.jamk.fi/core/50-Testaushallinta/yleistestaussuunnitelma/)
* Systeemityömenetelmänä Agile

## 4. Hinnat ja veloitukset

Kokonaishinta 81 000,00 € (sis. alv 24%), seuraavan aikataulun mukaisesti

* maksupiste		€	eräpäivä *
* E1	Sopimuksen allekirjoitus	26 000,00	14.9.2020
* E2	Päätestaussuunnitelma	15 000,00	16.11.2020
* E3	Hyväksyntätestaus	20 000,00	4.12.2020
* E4	Käyttöönotto	20 000,00	12.12.2020
* Maksun eräpäivä 7 vrk etapista




### Työn jakautuminen:

* Ohjaus + hallinto  5 %
* määrittely ja suunnittelu 40 %
* koodaus ja testaus 40 %
* dokumentointi 15 %


## 5. Toimittajan esittely

* Projektiryhmä Corpo Ration Corp. : [https://gitlab.labranet.jamk.fi/te-team1/site](https://gitlab.labranet.jamk.fi/te-team1/site)
* Lisätietoja [Projektiryhmän esittely](http://te-team1.pages.labranet.jamk.fi/core/10-Projektinhallinta/projektiryhman-esittely/)

## 6. Muuta huomioitavaa

*	rajoitukset: Aikaresurssi, legacy tuote
*	suomenkielinen/englanninkielinen
*	tarjous on voimassa 13.12.2020 saakka


Allekirjoitukset
15.9.2020

________________________
Reima Parviainen
Corpo Ration Corp. 
Leader

_________________________
Marko Rintämäki
Jyväskylän Ammattikorkeakoulu
Product Owner

__________________________
Teemu Kontio
Wimma Lab
		

## LIITTEET

Liite 1 	[WimmaForum Demovideo](https://youtu.be/MB022CtqAAU)
Liite 2 	[Projektisuunnitelma](http://te-team1.pages.labranet.jamk.fi/core/10-Projektinhallinta/projektisuunnitelma/)
Liite 3 	[Vaatimusmääritelmä](http://te-team1.pages.labranet.jamk.fi/core/20-Vaatimustenhallinta/vaatimusmaarittely/)
Liite 3     [Yleistestaussuunnitelma](http://te-team1.pages.labranet.jamk.fi/core/50-Testaushallinta/yleistestaussuunnitelma/)
