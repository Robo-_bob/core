# Corpo Ration Corp.
![Corpo Ration Corp.](https://gitlab.labranet.jamk.fi/te-team1/site/-/raw/master/public/img/corpologo.png)
> 'Mens sana in Corpo Ration sano.'

[Corpo Ration Corp. Website](http://te-team1.pages.labranet.jamk.fi/site/)

* [**RELEASE NOTE 4.12.2020**](http://te-team1.pages.labranet.jamk.fi/core/60-Palvelutuotanto/releasenote/)

<br>[WimmaForum](https://corporation.devopskoulutus.net)
<br>
<br>
<iframe width="560" height="315" src="https://www.youtube.com/embed/MB022CtqAAU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<br>
<br>

|  **Our team!**  | **Title** |  **LinkedIn**  | **GitLab** |
| :----------: | :------: | :-----------: | :------: |
| [Reima](http://te-team1.pages.labranet.jamk.fi/core/10-Projektinhallinta/projektiryhman-esittely/#reima_parviainen) | Team Leader | [LinkedIn](https://www.linkedin.com/in/reima-parviainen/) | [GitLab AA6135](https://gitlab.labranet.jamk.fi/AA6135) |
| [Sanni](http://te-team1.pages.labranet.jamk.fi/core/10-Projektinhallinta/projektiryhman-esittely/#sanni_jetsu) | Tester | [LinkedIn](https://www.linkedin.com/in/sanni-jetsu) | [GitLab AA6130](https://gitlab.labranet.jamk.fi/AA6130) |
| [Riku](http://te-team1.pages.labranet.jamk.fi/core/10-Projektinhallinta/projektiryhman-esittely/#riku_harkala) | Tester | [LinkedIn](https://www.linkedin.com/in/riku-harkala/) | [GitLab P1384](https://gitlab.labranet.jamk.fi/P1384) |
| [Sami](http://te-team1.pages.labranet.jamk.fi/core/10-Projektinhallinta/projektiryhman-esittely/#sami_kurula) | Tester | [LinkedIn](https://www.linkedin.com/sami-kurula) | [GitLab AA6127](https://gitlab.labranet.jamk.fi/AA6127) |
| [Konsta](http://te-team1.pages.labranet.jamk.fi/core/10-Projektinhallinta/projektiryhman-esittely/#konsta_leppanen) | Developer | [LinkedIn](https://www.linkedin.com/in/konsta-leppanen/) | [GitLab P1375](https://gitlab.labranet.jamk.fi/P1375) |
| [Sajid](http://te-team1.pages.labranet.jamk.fi/core/10-Projektinhallinta/projektiryhman-esittely/#sajid_howlader) | Developer | [LinkedIn](https://www.linkedin.com/in/sajidpabc/)| [GitLab AA0133](https://gitlab.labranet.jamk.fi/AA0133) |
| [Jukka](http://te-team1.pages.labranet.jamk.fi/core/10-Projektinhallinta/projektiryhman-esittely/#jukka_veijanen) | Developer | [LinkedIn](https://www.linkedin.com/in/jukkaveijanen/) | [GitLab K0416](https://gitlab.labranet.jamk.fi/K0416) |
| [Khaled](http://te-team1.pages.labranet.jamk.fi/core/10-Projektinhallinta/projektiryhman-esittely/#khaled_nuri) | Developer | [LinkedIn](https://www.linkedin.com/in/khaled-nuri-750207128/) | [GitLab AA6150](https://gitlab.labranet.jamk.fi/AA6150) |


* Contact us! AA####@student.jamk.fi

