# Testitulokset


* [Yleistestaus-suunnitelma](https://195.148.22.11/testlink/lib/results/printDocument.php?apikey=385291977369ca70e929bbb60918738959b3e911c0c7b36a93aa740561b37047&type=test_report&level=testproject&tproject_id=782&tplan_id=2259&header=y&summary=y&toc=y&body=y&passfail=y&cfields=y&metrics=y&author=y&requirement=y&keyword=y&notes=y&headerNumbering=y&format=0)
* [WimmaForum TestPlan v2.pdf](TestPlanReport2711.pdf)

## Tulokset 

* Viimeisimmät testit on ajettu versioilla: Kirahvi

## Tulosten upotus esimerkki (Lähde. Testlink)

[TestLink Charts](https://195.148.22.11/testlink/lib/results/charts.php?format=0&tplan_id=2259)
<iframe id="TestLink Charts"
    title="TestLink Charts"
    width="640"
    height="480"
    src="(https://195.148.22.11/testlink/lnl.php?apikey=1a3fab439652d5533067dc491074b5c151d0166be63f6eec5711106720e38973&tproject_id=782&tplan_id=1879&type=charts_basic">
</iframe>




## Hyväksyntätestaus

* [Hyväksyntätestaus](AcceptanceTesting2711.pdf)

## Järjestelmätestaus:

* Toiminnnalliset testejä ajettu monta kpl
* Ei-toiminnallisia testejä ajettu monta kpl
* Kattavuus 100 %
* Pass Rate %
* Failed 
* Raportoideut viat/bugit #1, #2, #123 
* Linkki ?

### Suorituskyky (Performance)

* TBD

### Tietoturva (Security) 

* [DataSec](DataSecTesting2711.pdf)

### Kuormitus (Load)

* TBD
