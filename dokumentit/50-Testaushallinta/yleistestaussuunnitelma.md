*Tämä dokumentti on elävä dokumenttipohja, jota päivitetään tilanteen mukaan..*
Versio 21.11.2018 By NarsuMan



# Yleistestaussuunnitelma - Master Test Plan

* [Yleistestaus-suunnitelma](https://195.148.22.11/testlink/lib/results/printDocument.php?apikey=385291977369ca70e929bbb60918738959b3e911c0c7b36a93aa740561b37047&type=test_report&level=testproject&tproject_id=782&tplan_id=2259&header=y&summary=y&toc=y&body=y&passfail=y&cfields=y&metrics=y&author=y&requirement=y&keyword=y&notes=y&headerNumbering=y&format=0)

v 2.0

Head of Testing - Reima Parviainen

# Yleistä 

TestLinkissä nähtävillä olemassa olevat testit ja niiden ajot.

## Testikohteesta (Test Target / System Under Test)

Testikohteena WimmaLab Forum realworld-conduit pohjan päälle rakennettu keskustelupalsta.

## Testauksen yleiset tavoitteet ja tärkeimmät tehtävät (Test goals and primary needs)

Päätavoitteenna tuotteen laadunvarmistus. Testejä tehdään kattavasti käytettävyyden ja tietoturvan näkökulmasta.

## Aikataulu (Schedule)

* [Projektisuunnitelma](../10-Projektinhallinta/projektisuunnitelma.md)

## Julkaisusuunnitelma (Release Plan)

* TBA

### Testattavat ominaisuudet (Tested Features)


| Ominaisuus | Prioriteetti | Use cases |
|:-|:-:|:-:|
| [Feature 1 - GDPR](../20-Vaatimustenhallinta/Featuret/feature01-gdpr.md) | P1 | [GDPR](use_cases/gdpr_hyvaksyminen.md) |
| [Feature 2 - salasananpalautus](../20-Vaatimustenhallinta/Featuret/feature02-salasananpalautus.md) | P2 |  [salasananpalautus](../20-Vaatimustenhallinta/use_cases/salasanan_palautus.md) |
| [Feature 3 - blacklist](../20-Vaatimustenhallinta/Featuret/feature03-blocklist.md) | P3 | [blocklist](../20-Vaatimustenhallinta/use_cases/kiellettyjen_sanojen_estäminen.md) |
| [Feature 4 - moderointipalvelu.md](../20-Vaatimustenhallinta/Featuret/feature04-moderointipalvelu.md) | P1 | [moderointi](../20-Vaatimustenhallinta/use_cases/moderointi_viestin_poisto.md) |
| [Feature 5 - käyttäjäroolit](../20-Vaatimustenhallinta/Featuret/feature05-kayttajaroolit.md) | P1 | [käyttäjäroolit](../20-Vaatimustenhallinta/use_cases/antaa_moderointi_oikeudet.md) |
| [Feature 6 - palaute](../20-Vaatimustenhallinta/Featuret/feature06-palaute.md) | P2 | [palaute](../20-Vaatimustenhallinta/use_cases/palautteen_anto.md) |
| [Feature 7 - backendlog](../20-Vaatimustenhallinta/Featuret/feature07-backendlog.md) | P2 | [backendlog](../20-Vaatimustenhallinta/use_cases/backend_log_katsominen.md) |
| [Feature 8 - googleanalytics](../20-Vaatimustenhallinta/Featuret/feature08-googleanalytics.md) | P3 |  |
| [Feature 9 - Docker](../20-Vaatimustenhallinta/Featuret/feature09-Docker.md) | P1 | |
| [Feature 10 - HTTPS](../20-Vaatimustenhallinta/Featuret/feature10-TLS.md) | P1 |  |
| [Feature 11 - Branding](../20-Vaatimustenhallinta/Featuret/feature11-branding.md) | P1 |  |
| [Feature 12 - Some](../20-Vaatimustenhallinta/Featuret/feature12-some.md) | P1 | [sosiaaliseen mediaan jakaminen](use_cases/sosiaaliseen_mediaan_jakaminen.md) |
| [OGFT01: Sign Up](../20-Vaatimustenhallinta/Featuret Conduitissa/ogft01-signup.md) |  |  |
| [OGFT02: Sign In](../20-Vaatimustenhallinta/Featuret Conduitissa/ogft02-signin.md) | |   |
| [OGFT03: Your Settings](../20-Vaatimustenhallinta/Featuret Conduitissa/ogft03-yoursettings.md) |  | |
| [OGFT04: Profile](../20-Vaatimustenhallinta/Featuret Conduitissa/ogft04-profile.md) | P1 | ) |
| [OGFT05: New Post](../20-Vaatimustenhallinta/Featuret Conduitissa/ogft05-newpost.md) | P1 |  |
| [OGFT06: Feed / Home](../20-Vaatimustenhallinta/Featuret Conduitissa/ogft06-feed.md) | P2 |  |
| [OGFT07: Tags](../20-Vaatimustenhallinta/Featuret Conduitissa/ogft07-tags.md) | P2 |  |
| [OGFT08: Edit Article](../20-Vaatimustenhallinta/Featuret Conduitissa/ogft08-edit.md) | P3 |  |
| [OGFT09: Delete Article](../20-Vaatimustenhallinta/Featuret Conduitissa/ogft09-delete.md) | P1 | |
| [OGFT10: Post Comment](../20-Vaatimustenhallinta/Featuret Conduitissa/ogft10-comment.md) | P1 |  |
| [OGFT11: Delete Comment](../20-Vaatimustenhallinta/Featuret Conduitissa/ogft11-rmcomment.md) | P1 |  |
| [OGFT12: Follow User](../20-Vaatimustenhallinta/Featuret Conduitissa/ogft12-follow.md) | P1 |  |
| [OGFT13: Unfollow User](../20-Vaatimustenhallinta/Featuret Conduitissa/ogft13-unfollow.md) | P1 |  |
| [OGFT14: Favorite Article](../20-Vaatimustenhallinta/Featuret Conduitissa/ogft14-favorite.md) | P1 |  |

## Testausympäristöt (Testing Environments)

* Visual Studio Code
* debuggeri zky v2.05
* Notepad ++
* Sublime Text Version
* gedit
* GNU nano
* Brackets
* Git for Windows
* Gitlab
* xed
* Firefox
* Chrome
* Brave Version 1.17.72 Chromium
* Microsoft Excel
* Microsoft PowerPoint
* Microsoft Teams
* LibreOffice Draw
* Ghostwriter

### Resurssit ja vastuut (Resources and ...)

| Resurssi | Vastuus | Muuta | Yritys |
|:-:|:-:|:-:|:-:|
| Reima | Manuaalinen testaus | Testaussuunnittelu | Corpo Ration Corp. | 
| Sami  | Testausautomaatio | Selenium | Corpo Ration Corp. | 
| Sanni | Toiminnallinen testaus | 
| Riku | Palvelin testaus | 

## Testaustasot (Testing Levels)

```plantuml
start
:Vaatimukset;
:Maarittely;
:Arkkitehtuurisuunnittelu;
:Moduulisuunnittelu;
:Ohjelmointi;
:Moduulitestaus;
:Integrointitestaus;
:Jarjestelmatestaus;
:Hyvaksymistestit;
end
```

### Hyväksyntätestaus (Acceptance Testing)

* [Hyväksyntätestaus](https://195.148.22.11/testlink/lib/results/printDocument.php?apikey=b42fb11b27361e803f1f45e76aceb20835bc2495103e1e1f310492e2a05b55ee&type=test_report&level=testproject&tproject_id=782&tplan_id=2271&header=y&summary=y&toc=y&body=y&passfail=y&cfields=y&metrics=y&author=y&requirement=y&keyword=y&notes=y&headerNumbering=y&format=0) 

### Järjestelmätestaus (System Testing)

* [Järjestelmätestaus](https://195.148.22.11/testlink/lib/results/printDocument.php?apikey=385291977369ca70e929bbb60918738959b3e911c0c7b36a93aa740561b37047&type=test_report&level=testproject&tproject_id=782&tplan_id=2259&header=y&summary=y&toc=y&body=y&passfail=y&cfields=y&metrics=y&author=y&requirement=y&keyword=y&notes=y&headerNumbering=y&format=0)

### Järjestelmän integraatio testaus (System Integration Testing)

* [Integraatiotestaus](https://195.148.22.11/testlink/lib/results/printDocument.php?apikey=385291977369ca70e929bbb60918738959b3e911c0c7b36a93aa740561b37047&type=test_report&level=testproject&tproject_id=782&tplan_id=2259&header=y&summary=y&toc=y&body=y&passfail=y&cfields=y&metrics=y&author=y&requirement=y&keyword=y&notes=y&headerNumbering=y&format=0)

### Moduli/komponenttitason testaus (Module / Component Testing)

* [Moduulitestaus](https://195.148.22.11/testlink/lib/results/printDocument.php?apikey=385291977369ca70e929bbb60918738959b3e911c0c7b36a93aa740561b37047&type=test_report&level=testproject&tproject_id=782&tplan_id=2259&header=y&summary=y&toc=y&body=y&passfail=y&cfields=y&metrics=y&author=y&requirement=y&keyword=y&notes=y&headerNumbering=y&format=0)

## Testisuunnittelusta

* [Yleistestaus-suunnitelma](https://195.148.22.11/testlink/lib/results/printDocument.php?apikey=385291977369ca70e929bbb60918738959b3e911c0c7b36a93aa740561b37047&type=test_report&level=testproject&tproject_id=782&tplan_id=2259&header=y&summary=y&toc=y&body=y&passfail=y&cfields=y&metrics=y&author=y&requirement=y&keyword=y&notes=y&headerNumbering=y&format=0)

## Testaus ja vianhallinnan prosessit

* [Yleistestaus-suunnitelma](https://195.148.22.11/testlink/lib/results/printDocument.php?apikey=385291977369ca70e929bbb60918738959b3e911c0c7b36a93aa740561b37047&type=test_report&level=testproject&tproject_id=782&tplan_id=2259&header=y&summary=y&toc=y&body=y&passfail=y&cfields=y&metrics=y&author=y&requirement=y&keyword=y&notes=y&headerNumbering=y&format=0)

* Yleiset hyväksyntä kriteerit
* Yleiset hylkäys kriteerit


## Valittu testausstrategia

* [Yleistestaus-suunnitelma](https://195.148.22.11/testlink/lib/results/printDocument.php?apikey=385291977369ca70e929bbb60918738959b3e911c0c7b36a93aa740561b37047&type=test_report&level=testproject&tproject_id=782&tplan_id=2259&header=y&summary=y&toc=y&body=y&passfail=y&cfields=y&metrics=y&author=y&requirement=y&keyword=y&notes=y&headerNumbering=y&format=0)

## Käytetyt testityövälineet ja ohjelmistot

* Visual Studio Code
* debuggeri zky v2.05
* Notepad ++
* Sublime Text Version
* gedit
* GNU nano
* Brackets
* Git for Windows
* Gitlab
* xed
* Firefox
* Chrome
* Brave Version 1.17.72 Chromium
* Microsoft Excel
* Microsoft PowerPoint
* Microsoft Teams
* LibreOffice Draw
* Ghostwriter

### Suorituskykytestaus - Performance Testing

* [Suorituskykytestaus](https://195.148.22.11/testlink/lnl.php?apikey=1a3fab439652d5533067dc491074b5c151d0166be63f6eec5711106720e38973&tproject_id=782&tplan_id=1879&type=test_plan)

### Toiminnallinentestaus - Functional Testing

* [Toiminnallinentestaus](https://195.148.22.11/testlink/lnl.php?apikey=1a3fab439652d5533067dc491074b5c151d0166be63f6eec5711106720e38973&tproject_id=782&tplan_id=1879&type=test_plan)

### Tietoturvatestaus

* [DataSec](https://195.148.22.11/testlink/lnl.php?apikey=1a3fab439652d5533067dc491074b5c151d0166be63f6eec5711106720e38973&tproject_id=782&tplan_id=1879&type=test_plan)

### Kuormitustestaus

* [Kuormitustestaus](https://195.148.22.11/testlink/lnl.php?apikey=1a3fab439652d5533067dc491074b5c151d0166be63f6eec5711106720e38973&tproject_id=782&tplan_id=1879&type=test_plan)


## Liitteet

* [LIITE1](https://195.148.22.11/testlink/lnl.php?apikey=1a3fab439652d5533067dc491074b5c151d0166be63f6eec5711106720e38973&tproject_id=782&tplan_id=1879&type=test_plan)

