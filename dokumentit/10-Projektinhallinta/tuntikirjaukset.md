***Tuntikirjaukset Conduit projektissa***

| Aika        | Sami | Reima | Riku | Sanni | Khaled | Jukka | Konsta | Sajid | Summa h | Hinta 50€/h |
| ----------- | ---- | ----- | ---- | ----- | ------ | ----- | ------ | ----- | ------- | ----------- |
| Sprint 00   | 8    | 8     | 8    | 8     | 0      | 0     | 0      | 0     | 32      | 1600        |
| Sprint 01   | 8    | 8     | 8    | 8     | 0      | 0     | 0      | 0     | 32      | 1600        |
| Sprint 02   | 8    | 8     | 8    | 8     | 0      | 0     | 0      | 0     | 32      | 1600        |
| Sprint 03   | 8    | 8     | 8    | 8     | 0      | 0     | 0      | 0     | 32      | 1600        |
| Sprint 04   | 8    | 8     | 8    | 8     | 0      | 0     | 0      | 0     | 32      | 1600        |
| Sprint 05   | 8    | 8     | 8    | 8     | 0      | 0     | 0      | 0     | 32      | 1600        |
| Sprint 06   | 8    | 8     | 8    | 8     | 0      | 0     | 0      | 0     | 32      | 1600        |
| Sprint 07   | 8    | 8     | 8    | 8     | 40     | 20    | 40     | 40    | 172     | 8600        |
| Sprint 08   | 8    | 8     | 8    | 8     | 40     | 20    | 40     | 40    | 172     | 8600        |
| Sprint 09   | 8    | 8     | 8    | 8     | 40     | 20    | 40     | 40    | 172     | 8600        |
| Sprint 10   | 8    | 8     | 8    | 8     | 40     | 20    | 40     | 40    | 172     | 8600        |
| Sprint 11   | 8    | 8     | 8    | 8     | 40     | 20    | 40     | 40    | 172     | 8600        |
| Loppu arvio |      |       |      |       |        |       |        |       | 1052    | 52600       |

<iframe width="480" height="400" frameborder="0" scrolling="no" src="https://jamkstudent-my.sharepoint.com/personal/aa6127_student_jamk_fi/_layouts/15/Doc.aspx?sourcedoc={45652711-86bc-43ea-b818-f8699b94e9c3}&action=embedview&wdAllowInteractivity=False&wdHideGridlines=True&wdHideHeaders=True&wdDownloadButton=True&wdInConfigurator=True"></iframe>


