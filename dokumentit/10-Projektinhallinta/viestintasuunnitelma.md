# Viestintäsuunnitelma

#	Johdanto

Dokumentissa tuodaan esille Team Corpo Ration Corp Conduit-projektin yhteydessä käytetyt viestintämenetelmät ja  kanavat. Vuoden 2020 koronapandemian vaikutuksesta lähikontaktiviestintä on minimoitu.

#	Projektin osapuolet

  1.	Projektiryhmän jäsenet   
  - Reima Parviainen    
  - Sanni Jetsu   
  - Riku Härkälä   
  - Sami Kurula    
  - Jukka Veijanen   
  - Konsta Leppänen   
  - Mohammed Howlader   
  - Khaled Nuri   
  2.	Projektin ohjaajat   
  - Marko "NarsuMan" Rintamäki   
  - Heli Vepsäläinen   
  - Jouni Huotari   
  3.	Toimeksiantajan edustajat    
  - Marko "NarsuMan" Rintamäki   
  - Teemu Kontio  
  - WimmaLab     

## Yleiset viestintäkanavat

Pääsääntöisesti projektin viestinnässä hyödynnetään Teams-alustaa. Tarvittaessa projektin jäsenet voivat olla toisiinsa yhteydessä myös sähköpostilla ja puhelimitse. 

## Sisäinen viestintä

Sisäisellä viestinnälla tarkoitetaan projektiryhmän jäsenten välistä viestintää. Sisäisellä viestinnällä varmistetaan, että projektiryhmän jäsenet ovat ajan tasalla projektin etenemisestä.   
Projektiryhmä on päättänyt käyttää yleisenä viestintäalustana Teams:ia. Ryhmä käyttää chat-toimintoa aktiivisesti projektin etenemisestä informoimisessa.
Palavereja pidetään viikottain, yleensä maanantaiaamuisin ja tarvittaessa perjantaisin. Ylimääräisiä tapaamisia voidaan pitää tarvittaessa.

## Viestintä ohjaajien kanssa

Projektin ohjaajilla on vapaa pääsy ryhmän viestintäkanavalle ja palavereihin. Näin varmistetaan ohjaajien esteetön pääsy seuraamaan ryhmän etenemistä, ja varmistetaan, että ryhmä voi tarvittaessa saada apua ohjaajilta lyhyelläkin varoitusajalla. Ohjaajien järjestämissä ohjaussessioissa on mahdollista selvittää pattitilanteita projektin ongelmatilanteissa. Marko Rintamäki pitää ohjaussessioita perjantaisin ja keskiviikkoisin. Äänitorvena projektiryhmän ja ulkopuolisten välillä toimii Team Leader.

# Viestintä toimeksiantajan edustajien kanssa

Teemu Kontio on tavattu Sprint03 aikana. Projektin ohjaaja Marko Rintamäki on yhteydessä toimeksiantajan edustajaan jatkuvasti, ja informoi projektiryhmiä tarvittaessa.

# Viestintä ulkopuolisten kanssa

Viestintä ulkopuolisten (kuten muiden projektiryhmien) kanssa tapahtuu pääosin Teams-palvelun kautta.

# Viestinnän seuranta ja arviointi

Viestinnän toimivuutta arvioidaan ryhmän sisällä jatkuvasti projektin edetessä. Tarvittaessa viestintäkeinoja muokataan osapuolten tarpeiden mukaiseksi.

# Lähteet

Alkuperäinen löytyy täältä! http://homes.jamk.fi/~huojo/opetus/IIZP2010/
