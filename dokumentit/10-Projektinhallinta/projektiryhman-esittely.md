# Projektiryhmän esittely

 
[Corpo Ration Corp.](http://te-team1.pages.labranet.jamk.fi/site/)


|  **Our team!**  | **Title** |  **LinkedIn**  | **GitLab** |
| :----------: | :------: | :-----------: | :------: |
| [Reima](#reima_parviainen) | Team Leader | [LinkedIn](https://www.linkedin.com/in/reima-parviainen/) | [GitLab AA6135](https://gitlab.labranet.jamk.fi/AA6135) |
| [Sanni](#sanni_jetsu) | Tester | [LinkedIn](https://www.linkedin.com/in/sanni-jetsu) | [GitLab AA6130](https://gitlab.labranet.jamk.fi/AA6130) |
| [Riku](#riku_harkala) | Tester | [LinkedIn](https://www.linkedin.com/in/riku-harkala/) | [GitLab P1384](https://gitlab.labranet.jamk.fi/P1384) |
| [Sami](#sami_kurula) | Tester | [LinkedIn](https://www.linkedin.com/sami-kurula) | [GitLab AA6127](https://gitlab.labranet.jamk.fi/AA6127) |
| [Konsta](#konsta_leppanen) | Developer | [LinkedIn](https://www.linkedin.com/in/konsta-leppanen/) | [GitLab P1375](https://gitlab.labranet.jamk.fi/P1375) |
| [Sajid](#sajid_howlader) | Developer | [LinkedIn](https://www.linkedin.com/in/sajidpabc/)| [GitLab AA0133](https://gitlab.labranet.jamk.fi/AA0133) |
| [Jukka](#jukka_veijanen) | Developer | [LinkedIn](https://www.linkedin.com/in/jukkaveijanen/) | [GitLab K0416](https://gitlab.labranet.jamk.fi/K0416) |
| [Khaled](#khaled_nuri) | Developer | [LinkedIn](https://www.linkedin.com/in/khaled-nuri-750207128/) | [GitLab AA6150](https://gitlab.labranet.jamk.fi/AA6150) |



## Reima Parviainen 

_Leader_ / _Head of testing_ / _Web master_ / Scrum master 

<img src="../kuvat/reima.png" alt="Reima"
	title="Reima" width="350"/>

### Aiemmat koulutukset:    
_Viittomakielentulkki_ _(2015)_          
_Puhevammaisten_ _tulkki_ _(2018)_       

### Vastuualueet:  
_Siten_ _huolto_   
_Testitapausten_ _huolto_    


## Sanni Jetsu 

_Viestintävastaava,_ _testaaja_   

<img src="../kuvat/sanni.png" alt="Sanni"
	title="Sanni" width="350"/>

### Aiemmat koulutukset:   
_Ylioppilastutkinto_ _(2020)_  

### Vastuualueet:    
_Dokumenttien_ _huolto_   


## Sami Kurula 

_Riskienhallintavastaava_ _testaaja_   
<img src="../kuvat/sami.png" alt="Sami"
	title="Sami" width="350"/>
  

### Aiemmat koulutukset:  
_Tietotekniikan_ _perustutkinto_  

### Vastuualueet:    
_Testaus_   


## Riku Härkälä

_Asiantuntija,_ _testaaja_   
 
 <img src="../kuvat/riku.png" alt="Riku"
	title="Khaled" width="350"/> 

### Aiemmat koulutukset:     
_Hotellivirkailija_ _(1995)_    
_Lukuisia_ _IT-alan_ _työvoimakoulutuksia_   

## Vastuualueet:
_CSC_   
_Testaus_   


## Khaled Nuri

_Ohjelmoija_   

  
 <img src="../kuvat/khaled.png" alt="Khaled"
	title="Khaled" width="350"/> 
  

### Aiemmat koulutukset:    
_Aineenopettajan_ _opinnot_ _(2009)_    

### Vastuualueet:  
_Koodaus_       



## Jukka Veijanen   

_Ohjelmoija_   
  
 <img src="../kuvat/jukka.png" alt="Jukka"
	title="Jukka" width="350"/> 


### Aiemmat koulutukset:      
_Master_ _of_ _Engineering,_ _Cyber_ _Security_      

### Vastuualueet:   
_Koodaus_       


## Konsta Leppänen   

_Ohjelmoija_   
 
 <img src="../kuvat/konsta.png" alt="Konsta"
	title="Konsta" width="350"/> 
  

### Aiemmat koulutukset:  
_Filosofian_ _maisteri,_ _pääaine_ _matematiikka_ _(2017)_       

### Vastuualueet: 
_Koodaus_      


## Sajid Howlader   

_Ohjelmoija_   

 <img src="../kuvat/sajid.png" alt="Sajid"
	title="Sajid" width="350"/> 


### Aiemmat koulutukset:    
_Filosofian_ _tohtori,_ _Ekologia_ _ja_ _evoluutiobiologia_        

### Vastuualueet:  
_Koodaus_     
