# Projektisuunnitelma

[1. Toimeksianto](#1_projektin_ja_lopputuotteen_kuvaus)

* Tausta ja lähtökohdat, tavoitteet ja tehtävät, rajaus ja liittymät, tulos

[2. Projektiorganisaatio](#2_projektiorganisaatio)

* Organisaation esittely, vastuut ja päätöksentekoprosessi

[3. Projektin vaiheet ja taloudelliset tavoitteet](#3_projektin_ajalliset_tavoitteet)

* Tehtäväkokonaisuudet, osittelu ja vaiheistus, välitulokset, aikataulut ja resurssissuunnitelmat, budjetti

[4. Laadun varmistus](#4_laadunvarmistus)

* Menetelmät, standardit, hyväksymismenettely, muutosten hallinta, dokumentointi, katselmoinnit, riskien hallinta, muut täydentävät suunnitelmat

[5. Tiedonvälitys ja projektin etenemisen seuranta](#5_tiedonvalitys_ja_projektin_etenemisen_seuranta_viestintasuunnitelma))

* Projektin aloitus, työtilat ja viestintävälineet, palaverikäytäntö ja yhteydenpito, raportointi ja tiedotus, projektikansio

[6. Projektin päättyminen](#6_projektin_paattyminen)
* Luovutus, käyttöönotto, ylläpito, projektin aineiston taltiointi, arkistointi, loppuraportti, projektin virallinen päättäminen

## 1 Projektin ja lopputuotteen kuvaus

Tässä dokumentissa kuvataan TTOS0800 -opintojaksolla toteutettavaa WimmaLabin Conduit-pohjaista keskustelualusta -ohjelmistoprojektia. Alla olevissa otsikoissa avataan tarkemmin taustaa, tavoitteita, tehtäviä, vaihejakoa, resursseja ja organisaatiota. Lopputuloksena tuotetaan esim. http://forum.wimmalab.org muotoinen sivusto, joka tulee osaksi wimmalab.org kokonaisuutta.

Mock Up
<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Fproto%2FuISbBcn9myJVnNOmBtkb6h%2FWimmaLab-Forum%3Fnode-id%3D3%253A4%26viewport%3D735%252C944%252C0.6867609620094299%26scaling%3Dscale-down" allowfullscreen></iframe>

## 1.1 Tausta ja lähtökohdat

WimmaLabin itsenäinen Foorumi/keskustelualusta tuli toimeksiantona TTOS0800 -opintojakson yrityksellemme Corpo Ration Corp.:lle. Pohjaksi on valittu maailmalla suosittu "Conduit"-palvelu, jonka ominaisuuksia aiotaan laajentaa paremmin WIMMALAB.org:ille sopivammaksi. Tilaajana WimmaLabin Teemu Kontio ja product ownerina Marko "Narsu" Rintamäki. Toimeksianto on annetu myös opintojakson kahdelle muulle yritykselle Bittikyylille sekä Karhukaisille.  

## 1.2 Tavoitteet ja tehtävät

* Tavoite on liittää Conduit-palvelu mukaan nykyiseen verkkosivuun oman domain nimen kautta. Esim. http://forum.wimmalab.org

Ohjemiston testaus ja laadunvarmistus (OTL) opiskelijat antavat Moderni Ohjelmistokehitys (MOK) opiskelijoille listan vaatimusmäärittelystä. MOK opiskelijat toteuttavat vaaditut ominaisuudet palveluun ja OTL opiskelijat huolehtivat tuotteen testauksesta ja laadunvarmistuksesta.
Lopputuote esitellään Product Owner Marko "Narsu" Rintamäelle, joka valitsee eri ryhmien toteutuksista parhaan vaihtoehdon. 

Tavoitteena toteuttaa toimeksiannossa tulleet ominaisuudet.

* P1 = Pakollinen
* P2 = Tarpeellinen
* P3 = Tehdään, kun tarve ilmenee

| Ominaisuus | Prioriteetti | Use cases |
|:-:|:-:|:-:|
| [Feature 1 - GDPR](Featuret/feature01-gdpr.md) | P1 | [GDPR](./use_cases/gdpr_hyvaksyminen.md) |
| [Feature 2 - salasananpalautus](Featuret/feature02-salasananpalautus.md) | P2 |  [GDPR](use_cases/gdpr_hyvaksyminen.md) |
| [Feature 3 - blacklist](Featuret/feature03-blocklist.md) | P3 | [GDPR](use_cases/gdpr_hyvaksyminen.md) |
| [Feature 4 - moderointipalvelu.md](Featuret/feature04-moderointipalvelu.md) | P1 | [GDPR](use_cases/gdpr_hyvaksyminen.md) |
| [Feature 5 - käyttäjäroolit](Featuret/feature05-kayttajaroolit.md) | P1 | [GDPR](use_cases/gdpr_hyvaksyminen.md) |
| [Feature 6 - palaute](Featuret/feature06-palaute.md) | P2 | [GDPR](use_cases/gdpr_hyvaksyminen.md) |
| [Feature 7 - backendlog](Featuret/feature07-backendlog.md) | P2 | [GDPR](use_cases/gdpr_hyvaksyminen.md) |
| [Feature 8 - googleanalytics](Featuret/feature08-googleanalytics.md) | P3 | [GDPR](use_cases/gdpr_hyvaksyminen.md) |
| [Feature 9 - Docker](Featuret/feature09-Docker.md) | P1 | [GDPR](use_cases/gdpr_hyvaksyminen.md) |
| [Feature 10 - HTTPS](Featuret/feature10-TLS.md) | P1 | [GDPR](use_cases/gdpr_hyvaksyminen.md) |


Lisäksi korjataan alkuperäiset Conduit -sovelluksen featureja. [Featuret Conduitissa](../20-Vaatimustenhallinta/Featuret-Conduitissa/ogft01-signup/)


## 1.3 Rajaus ja liittymät

Tarkoituksena on toteuttaa ainoastaan Narsun ja Teemun määrittämän ominaisuudet ja lähteä kehittämään pääasiassa niitä. Prioriteettiluokassaan alempien ominaisuuksien kehittäminen jätetään myöhemmälle tai jätetään pois toteutuksesta.

Suurin rajoittava tekijä projektissa on se, että Corpo Ration Corp. koostuu opikelijoista, ja suuri osa aikaresurssista menee uusien asioiden opiskeluun. OTL-kurssilaisilla pyörii samanaikaisesti myös muita opintojaksoja.

Conduit -toteutetaan yksikielisenä, sillä kaksikielisen käyttöliittymän toteuttaminen ei ole vaatimuslistauksessa.

## 1.4 Oikeudet

Eri osapuolten oikeudet on määritelty [projektisopimuksessa](../10-Projektinhallinta/projektisopimus.md). 

## 1.5 Termit ja määritelmät

* [Featuret](../20-Vaatimustenhallinta/Featuret) termillä tarkoitetaan [toimeksiantajan](../20-Vaatimustenhallinta/Profiilit/aisiakas.md) tilaamia ominaisuusvaatimuksia.
* [Featuret Conduitissa](../20-Vaatimustenhallinta/Featuret-Conduitissa) termillä tarkoitetaan alkuperäisiä RealWorld Conduit version ominaisuuksia.
* [Toimeksiantajalla](../20-Vaatimustenhallinta/Profiilit/aisiakas.md) tarkoitetaan WimmaLabin Teemu Kontiota.
* [Projektiorganisaatiolla](../10-Projektinhallinta/projektiryhman-esittely.md) tarkoitetaan opiskelijoista koostuvaa Corpo Ration Corp. harjoitusyritystä.

# 2. Projektiorganisaatio

## 2.1 Organisaation esittely

Projektin organisaation kuuluu Jyväskylän ammattikorkeakoulun  opettajia, projektihenkilökuntaa opiskelijaa, projektiryhmän ohjaajat sekä toimeksiantajan edustajat. Organisaatiokaavio on esitetty liitteessä <X>.”

**Projektiryhmä**

[Projektiryhmän esittely](../10-Projektinhallinta/projektiryhman-esittely.md) 

[Corpo Ration Corp. Website](http://te-team1.pages.labranet.jamk.fi/site/)

[Corpo Ration Corp. Core](http://te-team1.pages.labranet.jamk.fi/core/)


**Johtoryhmä**

JAMK:in IT-instituuttia edustaa Marko "Narsu" Rintamäki.

WimmaLabia edustaa Teemu Kontio.

Corpo Ration Corpin edustajana toimii scrum master Reima Parviainen.

**Tukiryhmä**

* Marko "Narsu" Rintamäki - Product owner ja opintojakson lehtori
* Teemu Kontio - WimmaLabin yhteyshenkilö
* Heli Vepsäläinen - Yleinen scrum master
* Jouni Huotari - Yleinen apu
* Anu Pelkonen - Henkinen tuki
* Juho Pekki - Koodauksen tuki

## 2.2 Vastuut ja päätöksentekoprosessi

* Marko "Narsu" Rintamäki - Product owner ja opintojakson lehtori - Vastuussa asiakkaan (WimmaLabin) suuntaan viestinnästä.
* Teemu Kontio - WimmaLabin yhteyshenkilö - Noudattaa projektisopimuksessa määriteltyjä kohtia.
* Reima Parviainen - Corpo Ration Corp. scrum master - Vastuussa viestinnästä johtoryhmän suuntaan, sekä tiimin työtehtävien jakaminen.
* Sanni Jetsu - Corpo Ration Corp. viestintävastaava - Vastuussa projektihallintapohjan ylläpidosta sekä GitLab issueiden jaosta.
* Sami Kurula - Corpo Ration Corp. ns. kirjanpitovastaava - Huolehtii työtuntien listauksesta sekä laskutuksesta.
* Riku Härkälä - Corpo Ration Corp. SysAdmin - Vastuussa CSC-palvelimesta sekä MOK tiimin ajan tasalla pitämisestä.
* Jukka Veijanen - Corpo Ration Corp. - Koodauspuoli.
* Konsta Leppänen - Corpo Ration Corp. - Koodauspuoli.
* Sajid Howlander - Corpo Ration Corp. - Koodauspuoli.
* Khaled Nuri -  - Corpo Ration Corp. - Koodauspuoli.

# 3. Projektin ajalliset tavoitteet

## 3.1 Osittaminen ja vaiheistus

Projektin osittamisella tarkoitetaan projektin jakamista selkeisiin osakokonaisuuksiin ja niitä vastaaviin toteutuskokonaisuuksiin (osaprojekteihin, vaiheisiin, tehtäväkokonaisuuksiin ja tehtäviin). Tutkimus- ja kehitysprojektien etenemiselle on tyypillistä lopputuloksen muodostuminen ja tavoitteen tarkentuminen vaihe vaiheelta. Projektin osituksen tulee perustua tähän lähtökohtaan (koskee myös IT-instituutin opiskelijaprojekteja). 

Projektin elinkaari voidaan jakaa erityyppisiin vaiheisiin. Kussakin vaiheessa tuotetaan määrätyt tuotteet, kuten selvitys, suunnitelmat, prototyyppi, laite jne. Kunkin vaiheen loppuun sovitaan arviointi, hyväksyntä tai katselmointi.

Ohjelmistoprojekti jakautuu tyypillisesti seitsemään vaiheeseen: perustaminen, esitutkimus, analyysi, suunnittelu, toteutus, testaus ja lopettaminen. Joskus esitutkimus on oma projektinsa, joskus analyysi sisällytetään suunnitteluun jne. Testaus ei välttämättä ole oma vaiheensa, vaan se sisältyy kaikkiin vaiheisiin.

Edetään inkrementaalisesti eli ensin suunnitellaan ja toteutetaan yksi asia kokonaisuudessaan ennen kuin edetään seuraavaan asiakokonaisuuteen. Ei ole yhtä ainutta ”oikeaa” vaihejakoa, mutta jos toimeksiantajalla on oma menetelmänsä ja siihen liittyvät mallipohjat, niin opiskelijaprojekteissa käytetään ensisijaisesti niitä. Yhä useammin käytetään ketterää sovelluskehitystä eli ohjelmisto tehdään 1-4 viikon sprinteissä.

Huom.: Seuraavassa on esitetty käynnistys- ja lopetusvaiheet. Kaikista projektin vaiheista, niiden kestoista ja työmääristä laaditaan myös nk. Gantt-kaavio (liitteenä), jossa näkyy myös vaiheiden väliset riippuvuudet ja tärkeimmät etapit (esim. johtoryhmän kokouspäivämäärät).



* [Sprint 00 14.9.2020-20.9.2020](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/2) Käynnistys

Projektin käynnistämiseen kuuluu olennaisesti projektisuunnittelu ja suunnitteludokumenttien laatiminen sekä yhteydenpitokäytänteiden luominen toimeksiantajayrityksen kanssa. Vaiheen aikana tehdään esim. ryhmän webbisivut, tutustutaan tarkemmin toimeksiantoon, aloitetaan kohdealueeseen perehtyminen ja laaditaan projektisuunnitelma yhteistyössä toimeksiantajan edustajien kanssa. Vaiheen aikana muodostetaan johtoryhmä, pidetään 1. johtoryhmän kokous sekä allekirjoitetaan projektisopimus.
”Vaiheen tuloksia ovat ryhmän imagon (nimi, logo ym.) luominen, webbisivut tms. sekä projektisopimus liitteineen.”

* [Sprint 01 21.9.2020-27.9.2020](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/3) 

Projektitoimintaan tutustuminen, webbisivujen ja projektinhallintasivujen päivitys.


* [Sprint 02 28.9.-4.10.2020](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/4) 

Vaatimusmäärittely, toimeksianto, riskienhallinta.

* [Sprint 03 5.10.-11.10.2020](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/5) 

TestLink tunnuksien tekeminen ja testitapauksiin tutustumista. Tuntikirjausten aloitus.

* [Sprint 04 12.10-18.10.2020](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/6)

Tärkeimmät Use Caset kirjattu. Test casejen määrittely aloitettu. 

* [Sprint 05 19.10-25.10.2020](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/14)

Conduit CSC-palvelimelle. Toiminnalliset/ei-toiminnalliset vaatimukset kirjattu. Riskienhallinta ja viestintäsuunnitelma valmiiksi. User Story tutustumista.

* [Sprint 06 26.10-1.11.2020](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/7)

Tarkistuslistan läpikäyntiä ennen E1-katselmointia. Yleistestausuunnitelman aloitus. Projektisopimuksen sisältö tarkistettu.

* [Sprint 07 2.11-8.11.2020](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/8)

* [Sprint 08 9.11-15.11.2020](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/9)

* [Sprint 09 16.11-22.11.2020](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/10)

* [Sprint 10 23.11-29.11.2020](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/11)

* [Sprint 11 30.11-11.12.2020](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/15)

* [Sprint End 12.12-13.12.2020](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/12)

Lopetus 12.12.2020 – 13.12.2020 (14 h)

”Lopettamisvaihe sisältää projektin päättämiseen liittyvät toimenpiteet. Vaiheen aikana projektiryhmä laatii projektin loppuraportin ja esityksen johtoryhmälle. Vaiheen aikana luovutetaan projektin tulos toimeksiantajalle, pidetään viimeinen johtoryhmän kokous viikolla X sekä puretaan projektin organisaatio. Lopettamisvaiheen tuloksena on projektin loppuraportti.”

## 3.2 Projektin alustavat kustannusarvio

[Tuntikirjaukset](../10-Projektinhallinta/tuntikirjaukset.md) sivulta löytyy työtunnit listattuna sekä kustannusarvio.

# 4. Laadunvarmistus

* Repositoriohallinassa käytössä [GitLab](https://gitlab.labranet.jamk.fi/te-team1/)
* Testaushallinnassa käytössä [TestLink](https://195.148.22.11/testlink/index.php)
* Palvelimena [CSC](https://my.csc.fi/)
* Ulkoasustandardit [WimmaLab Black Book](http://wimmalab.pages.labranet.jamk.fi/guides-and-info/Black-Book-1.1/)
* MockUp -työkaluna [Figma](https://www.figma.com/file/uISbBcn9myJVnNOmBtkb6h/WimmaLab-Forum?node-id=0%3A1)
* GitLab projektinhallintarunkona [OPF-Core](https://gitlab.labranet.jamk.fi/ttc2070-2021/opf-core-template-v2)

## 4.1 Väli- ja lopputulosten hyväksymismenettely

* Hyväksyntätestit
* Narsu hyväksyy
* Teemu hyväksyy

## 4.2 Muutosten hallinta

* GitLabin repositoriossa seuraamme aktiivisesti versiokehitystä.

## 4.3 Dokumentointi

Kaikki dokumentointi löytyy [https://gitlab.labranet.jamk.fi/te-team1/core](https://gitlab.labranet.jamk.fi/te-team1/core)
[https://gitlab.labranet.jamk.fi/te-team1/source](https://gitlab.labranet.jamk.fi/te-team1/source)

## 4.4 Riskien hallinta

[Riskienhallintasuunnitelma](../10-Projektinhallinta/riskienhallintasuunnitelma.md)


## 4.5 Katselmointikäytäntö

[Julkaisusuunnitelma](..\40-Julkaisusuunnittelu\julkaisusuunnitelma.md)

Linkit katselmointipöytäkirja pohjiin

## 4.6 Projektisuunnitelmaa täydentävät suunnitelmat

Tässä kohdassa mainitaan, mitä täydentäviä suunnitelmia on käytettävissä tai aiotaan projektin kuluessa laatia (esim. viestintä-, riskienhallinta-, testaus- ja käyttöönottosuunnitelma).

* [Vaatimusmäärittely](http://te-team1.pages.labranet.jamk.fi/core/20-Vaatimustenhallinta/vaatimusmaarittely/)
* [Riskienhallintasuunnitelma](http://te-team1.pages.labranet.jamk.fi/core/10-Projektinhallinta/riskienhallintasuunnitelma/)
* [Viestintäsuunnitelma](http://te-team1.pages.labranet.jamk.fi/core/10-Projektinhallinta/viestintasuunnitelma/)


## 4.7 Suunnitelmien tarkistus- ja päivitysajankohdat 

Maanantaisin ja perjantaisin.

## 4.8 Projektin keskeyttämiskriteerit

Oikeaoppiseen projektisuunnitelmaan kuuluu myös projektin keskeyttämiskriteerit. Näitä ei kuitenkaan opiskelijaprojekteissa käytetä, koska projekteissa käytetään tietty tuntimäärä tuloksen tekoon ja tulos luovutetaan sellaisena, kun se opintojakson päättyessä on. Projektiryhmä tekee kuitenkin jatkokehityssuunnitelman, josta mahdollinen uusi projekti jatkaa.

# 5. Tiedonvälitys ja projektin etenemisen seuranta (viestintäsuunnitelma)

[Viestintäsuunnitelma](..\10-Projektinhallinta\viestintasuunnitelma.md)

# 6. Projektin päättyminen

## 6.1 Lopputuotteen luovutus, käyttöönotto

Projektin lopputuote tulee myös dokumentoida järkevällä tasolla. Osana lopputuotetta saattaa olla asiakkaalle tarjottavaa käyttöönottokoulutusta ja mahdollisesti asennus- tai käyttöönotto­palvelua. Mikäli koulutuksen rooli projektin kannalta on huomattava (esimerkiksi ohjelmiston käyttäjät eivät ole olleet mukana projektissa ja eivät tiedä miten järjestelmä toimii) tulee projektisuunnitelmaan liittää suunnitelma asiakkaalle tarjottavasta koulutuksesta. Lisäksi jos on tarpeen, tulee projektisuunnitelmaan liittää myös asennussuunnitelma ja käyttöönottosuunnitelma.

## 6.2 Projektin tuottaman aineiston taltiointi, arkistointi ja säilytysaika

”Projektiryhmien dokumentaatiosta IT-instituutille jäävä osa tallennetaan GitHubiin.” Toimeksiantajan tulee selkeästi määritellä, mitkä dokumentit voidaan jättää opiksi seuraaville projekteille. Tyypillisesti eri suunnitelmat ja loppuraportti ovat tällaisia dokumentteja. 

## 6.3 Projektin virallinen päättäminen

On tärkeää määritellä milloin, mihin tai miten projekti päättyy. Projektin päätös voi olla tietty päivämäärä, tietty tuotteen valmiusaste, tietty työtuntimäärä, tietty kulutettu rahasumma, kun asiakas ottaa tuotteen käyttöön, takuuaika on umpeutunut tai kun asiakas hyväksyy tuotteen.

”Projekti päättyy 13.12.2020, jolloin projektisopimuksen voimassaoloaika päättyy.”

## 6.4 Lopetustilaisuus

Yleensä projektit päätetään yhteiseen päätösseminaariin. Tähän kirjataan osallistujat ja ajankohta. 

## 6.5 Projektin loppuraportti

Projektin loppuraportti laaditaan viimeiseen johtoryhmän kokoukseen mennessä.

## Liitteet

Projektisuunnitelmaa täydentävät suunnitelmat esitetään liitteenä.

* [Viesintäsuunnitelma](http://te-team1.pages.labranet.jamk.fi/core/10-Projektinhallinta/viestintasuunnitelma/)
* [Riskienhallinta](http://te-team1.pages.labranet.jamk.fi/core/10-Projektinhallinta/riskienhallintasuunnitelma/)
