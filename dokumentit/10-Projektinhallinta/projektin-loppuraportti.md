# Projektin loppuraportti



# 1.	JOHDANTO	

 Tämä dokumentti sisältää yrityksen [Corpo Ration Corp.](http://te-team1.pages.labranet.jamk.fi/site/) projektin loppuraportin. Projektin aikana yritys on työstänyt asiakkaan tilaamaa foorumipalvelua. Projektin pituus oli 11 viikkoa. Loppuraportti sisältää tietoa projektin aikataulutuksen toteutuksesta sekä yrityksen itsearvioinnin.

# 2.	TEHTÄVÄ, TAVOITE, TULOKSET



## 2.1.	Yhteenveto projektin toteumasta


 Projektin aikana yrityksen oli tarkoitus luoda [WimmaLabin](wimmalab.org) keskustelufoorumille uusia ominaisuuksia. Näihin ominaisuuksiin kuului muun muassa HTTPS-yhteys, Feedback-laatikko ja palvelun kontitus. Koko lista toivotuista ominaisuuksista löytyy Liitteet-osiosta. Uusien ominaisuuksien lisäksi yrityksen toivottiin korjaavan palvelussa ilmaantuvia ongelmia ja muokkaavan foorumin ulkonäköä WimmaLabin mukaiseksi.   
 Corpo Ration Corp. onnistui luomaan valitsemansa ominaisuudet keskustelualustalle projektin aikana sovitun aikataulun mukaisesti. 
Valittuihin ominaisuuksiin kuului:
* GDPR   
* HTTPS   
* Docker   
* Feedback   
* Docker compose-up   
* Brandays   
* Referenssi WimmaLabin sivuille   
* Google Analytics   
* Blacklist   
* Backend logittaminen   
* Tietoturva -audit   
* Varmistusjärjestelmä   

## 2.2.	 Projektin onnistuminen (suunnitelma vs. toteutuma)

 Corpo Ration Corp. loi prosessille aikataulutuksen [projektisuunnitelmassa](http://te-team1.pages.labranet.jamk.fi/core/10-Projektinhallinta/projektisuunnitelma/), josta pyrittiin pitämään kiinni. Aikatauluista joduttiin ajoittain joustamaan haastavan tehtävän työstämisen takia. Yritys koki silti kirivänsä menetetyn ajan nopeasti kiinni, eikä suurta työkuormaa kasautunut projetin loppuun.   
 Projektin sisäiset aikaresurssit olivat ajoittain tiukilla. Aikataulussa pystyttiin silti pidättäytymään, sillä suurempien tehtävien suorittamiseen hyödynnettiin ylijäänyttä aikaa kevyemmistä tehtävistä. Osa projektiryhmästä joutui työskentelemään hyvin pitkiä päiviä haasteiden kimpussa. Projektihallintaan ryhmä ei kokenut tarvitsevansa yhtä paljon aikaa kuin sisällön toteutukseen.   
 Projektin ulkopuolisia resursseja kuten katselmoinnit ja konsultaatiot olivat riittoisat projektiryhmälle. 

# 3.	ONGELMAT JA NIIDEN RATKAISUT

* Kuvatkaa ongelmat projektin suunnittelussa ja toteutuksessa (mistä johtui, miten ratkaistiin, mitä olisi pitänyt tehdä toisin ja miten)

## 3.1.	Ongelmat suunnittelussa

 Suurimmalla osalla ryhmän jäsenistä ei ollut kokemusta projektitoiminnasta ICT-alalla, joten tuntemattomien dokumenttien luonti oli ajoittain haastavaa. Projektipäällikkö avusti dokumenttien kirjoittamisessa, jos ryhmä koki sen vaikeaksi.

## 3.2.	Ongelmat toteutuksessa

 Toteutuksessa ryhmä kohtasi seuraavia ongelmia:
 * Reverse Proxyn säätäminen. Apua pyydettiin niin muilta ryhmistä, googlesta kuin myös ohjaajilta. Kurssiopettajan avulla ongelma saatiin ratkaistua. Ohjauskoodista puuttui kolmen kirjaimen mittainen loppu. (127.0.0.1:3000/ -> 127.0.0.1:3000/api)   
 * Docker localhost. Osalla ryhmän jäsenistä oli ongelmia Dockerin ajamisessa lokaalisti uuden aiheen takia. Perehtyminen ja devien avulla myös testauspuolen Dockerit saatiin korjattua.   
 * Backupin crontab. Ei onnistunut ajastetusti ajamaan Backup scriptiä. Korjauksen jälkeen backupit otetaan mongodb:stä kahdesti päivässä.   
 * Lokitus. Lokitus ei suostunut toimimaan syntaksin mukaisella koodilla. Internetin syövereistä löytyi vinkki rakenteen muuttamiseen, joka loi tiimille toimivan loggerin.     
 * Tietoturva-audit. Audittien ajaminen docker buldin yhteydessä aiheutti tiimille haasteita, sillä riippuvuuksien lisääminen oli ongelmaista ja kaatoi buildit. Ongelma saatiin ratkaistua speksien selailulla ja Googlen avulla.
 
## 3.3.	Muut ongelmat tai toteutuneet riskit ja niiden käsittely

 Projektin alussa yksi projektiryhmän jäsen joutui keskeyttämään projektin, jonka seurauksena ryhmän tuottavuus laski. Ongelman ratkaisuna toimi projektin uudelleenaikatauluttaminen.   
 Osa projektissa käytettävistä työkaluista koettiin epäkäytännölliseksi. Etenkin Testlinkin käyttö jakoi mielipiteitä ryhmässä. Testlinkin frontend koettiin vanhanaikaiseksi, käyttö monimutkaiseksi ja oikeuksien jakaminen turhan haastavaksi. Työkaluun perehtyminen oli aikavievää, mutta ratkaisi ryhmän kohtaamat ongelmat.

# 4.	YHTEENVETO

## 4.1.	Keskeiset opit

Opitut asiat vaiheittain:

**Suunnitteluvaihe:**   
* Projektityöskentely   
* Projektikäytänteet   
* Ketterän kehtiyksen käytänteet   
* Dokumentointi   

**Toteutuvaihe:**
* Docker   
* Repojen rakenne   
* Reverse Proxy   
* Työnjako   
* Vahvuuksien hyödyntäminen   

**Best** **practises**:   
* Näytön jakaminen ja yhteinen pohtiminen meetissä   
* Päivittäiset scrumit   
* Gitlab Boards   
* Gitlab Branches   

## 4.2.	Itsearviointi

### 4.2.1.	Ryhmätyö

*	Projektipäällikkyys toteutui ryhmässä hyvin. Tiimi pystyi luottamaan siihen, että joku otti kontrollin, jos asiat lähtivät leviämään.   
*	Erilaisuutta hyödynnettiin ryhmässä laadukkaasti. Erilaiset taidot ja intressit vaikuttivat työnjaossa vahvasti.   
*	Ongelmia ratkaistiin yhdessä heti niiden ilmaantumisen jälkeen. 
*	Työ jakaantui ajoittain epätasaisesti, jos vaativampia taskeja oli tietyllä sprintillä enemmän.   
*	Ohjaajien tarjoamaa apua hyödynnettiin aina tarvittaessa. Tietoa haettiin myös aktiivisesti muilta ryhmiltä ja netistä.   
*	Ryhmäytyminen oli Corpo Ration Corp.issa nopeaa ja vaivatonta. Ryhmän jäsenet pelasivat hyvin yhteen alusta asti.   
*	Vaikka kriisejä tuntui ryhmässä olevan suuri määrä, niistä selvittiin yhdessä. Kriisit eivät lannistaneet ryhmää, vaan onnistumiset hioivat tiimiä entistä paremmin yhteen.   
*	Ryhmä onnistui ottamaan tulleen kritiikin hyvin vastaan, ja kehittämään toimintaansa sen perusteella etenpäin.  

### 4.2.2.	Suunnitelmallisuus (projektityöskentely) 

*	Ryhmälle annettiin suuri lista ominaisuuksia, joita palveluun haluttiin. Corpo Ration Corp. valitsi ominaisuuksista tärkeimpiä, mutta lähti rohkeasti kokeilemaan kaikkien toteutusta.   
*	Lista tehdysitä ominaisuuksista löytyy dokumentin alkuosasta. Ryhmä on tyytyväinen lopputulokseensa.   
*	Muiden ryhmän jäsenten toimintaa valvottiin ja tarkastettiin aina ennen muutosten tekemistä. Näin varmistettiin, ettei virheellinen koodi päädy tuotantoon.   
*	Projektitoiminnan dokumentointi jäi ryhmällä vähäisemmäksi. Kokouksista ei tehty dokumentteja, mutta tärkeimmät huomioit kirjoitettiin keskustelukanavalle.   
*	Projektinhallinta koettiin onnistuneeksi. Projektissa vallitsi jatkuvasti tietynlainen järjestys, eikä projekti päässyt leviämään käsiin sen asiosta.   

### 4.2.3.	Vuorovaikutus

*	Yhteydenpito eri sidosryhmien välillä oli vähäistä, mutta riittävää. Tuotteesta päästiin keksustelemaan asiakkaan kanssa, ja myös ulkopuolisia tulevaisuuden käyttäjiä kuultiin. Projektiryhmän sisäinen vuorovaikutus oli vahvaa.   
*	Toimeksiantajalta koettiin saavan tarpeeksi tietoa projektista. Jos tiedon määrä tuntui vähäiseltä, sitä pääsi kysymään matalalla kynyksellä.   
*	Apua ja tietoa oli helppo hakea ohjaajilta, sillä ohjaajat olivat elposti lähestyttäviä ja avuliaita.   
*	Ryhmässä näkyi tasainen, positiivinen asenne. Syinä oli muun muassa ryhmän tuki, saatavilla oleva apu ja yhteisymmärrys.   
*	Ryhmän jäsenten kiireet otettiin huomioon, ja ryhmä tiedotti poissa olleita tehdyistä päätöksistä.   
*	Yhteydenpitoon käytettiin pääosin Teams-palvelua. Myös sähköpostia käytettiin yksittäisissä tapauksissa. Palaverit koettiin hyödylliseksi ajankäytöksi, sillä ryhmä sai eniten irti palavereista. Viestintä tapahtui paljon myös viestien kautta.   

### 4.2.4.	Asenne

*	Ryhmän asenne annettuun tehtävään oli innokas ja positiivinen. Ryhmä koki annetun tehtävänannon sopivan haastavana, joka vaikutti vahvasti ryhmän jaksamiseen ja tavoitteisiin.   
*	Oppimishalu oli Corpo Ration Corp.in sisällä vahva.   
*	Osa ongelmista koettiin turhan haastavana, mutta ryhmän tuki antoi energiaa ongelman selvittämiseen. Ongelmat koettiin pääosin haasteina, joista kyllä selvitään.   
*	Suunnitteluvaiheen loppupuolella projekti tuntui uuvuttavalta. Dokumentoinnista siirtyminen tekemiseen antoi lisäpuhtia tehtävään.
*	Tiimi haki jatkuvasti palautetta toimeksiantajalta, sillä projekti haluttiin tehdä toimeksiantajan toiveiden mukaisesti. 

### 4.2.5.	Tulos

*	Projektin tuloksena ryhmä on luonut toimivan keskustelufoorumin.  
*	Tuotos koetaan toimivana, mutta osittain edelleen puutteellisena. Foorumiin tehdyt parannukset ovat silti huomattavia.     
*	Projektiryhmän aineettomia tuloksia ovat vahva ryhmähenki ja asenteenmuutos.   


# LIITTEET

[Projektihallintasivu](http://te-team1.pages.labranet.jamk.fi/core/)   
[Projektisuunnitelma](http://te-team1.pages.labranet.jamk.fi/core/10-Projektinhallinta/projektisuunnitelma/)   