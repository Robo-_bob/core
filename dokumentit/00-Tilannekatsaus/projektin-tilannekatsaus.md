# Projektin tilannekatsaus


<iframe width="560" height="315" src="https://www.youtube.com/embed/MB022CtqAAU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## Sprint ja Gitlab Issuet

* CURRENT SPRINT 11

* [Sprint 11](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/15)
* [Issue Board -näkymä](https://gitlab.labranet.jamk.fi/te-team1/core/-/boards/5577?milestone_title=Sprint%2011&)
* [Issue Backlog](https://gitlab.labranet.jamk.fi/te-team1/core/-/milestones/1)


## Mitä saatu aikaan

* **Suurin osa featureista ajettu tuotantoon**
* **Viimeistelyä ja typojen korjailua**
* **Tuotteen esittelyyn valmistautumista**
* **Videoesittely valmis**
* **PowerPoint esitelmä valmis**
* **Sitet päivitetty markkinointikelpoisiksi**
* *https://corporation.devopskoulutus.net/ Ensimmäinen demo saatu online*
* *Ulkoasu muokattu*
* *Repositoriossa fiksailua*
* *Repositorio jaettu ReactFrontiin ja NodeBackiin*
* *Pipeline saatu toimimaan*
* *GDPR osuus aloitettu*
* *Doorbell.io implementoitu onnistuneesti*
* *Projektisuunnitelma viimeistelty*
* *Vaatimusmäärittely siistitty*
* *Projektiryhmän esittely ja site päivitetty*
* *E1-katselmointi*
* *Projektisopimus katsottu kuntoon*
* *TestLink hallussa. Testipuut luotu ja testitapauksia testitapauksia alettu luoda.*
* *Featureja tarkennettu.*
* *Riskienhallintasuunnitelmaa tehty.*
* *CSC otettu haltuun ja odottaa kontitusta.*
* *TestLink palveluun tutustuttu*
* *Asiakkaalta saatu tarkempi kuvaus mitä on tilattu*
* *Työtuntien kerääminen aloitettu*
* *MOK -ryhmäläiset liitetty osaksi tiimiä*
* *Use caseja alettu kartoittaa*
* *Vaatimusmäärittely aloitettu*
* *Projektisuunnitelma aloitettu*
* *Viestintäsuunnitelma aloitettu*
* *Suunnitteluvaiheen läpikäymistä ryhmänä*
* *Ominaisuuksien ja toiminallisuuksien tarkastelu*
* *Sidosryhmien tarkastelu*
* *Ryhmäytyminen ja roolijako*
* *Projektitoiminnan perusteet käyty läpi*
* *Vaatimusmäärittely saatu aluilleen*
* *Sivut (niin OPF kuin site) päivitetty ryhmän näköiseksi*

## Kohdatut ongelmat

* **Testiympäristön pystytyksessä aluksi hikottelua**
* *Frontissa joku linkitys pielessä. Source cloonataan ehkä uusiksi*
* *Repositorion pipeline ongelmat*
* *CSC -palvelimen kanssa painimista*

## Mitä seuraavaksi


* Seuraava sprint: End
* Tuotteen esittely
* [Sprint End - milestone](https://gitlab.labranet.jamk.fi/te-team1/core/-/boards/5220?milestone_title=Sprint%20End&)

## Projektin kokonaiskulut 4.12.2020   

[Tuntikirjaukset](http://te-team1.pages.labranet.jamk.fi/core/10-Projektinhallinta/tuntikirjaukset/)

