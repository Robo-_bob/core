# TUOTANTO

## Mistä lötyy tuotanto-palvelin?

Palvelun tuotanto versio löytyy osoitteesta [https://corporation.devopskoulutus.net/](https://corporation.devopskoulutus.net/)

## Palautetta

<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSd4JtW5YqFGSCOyW3_I0nb6Y7yYGAhObAA7XpbYZpkRhynmJQ/viewform?embedded=true" width="640" height="1126" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>

## Mistä löydän lähdekoodit ja suoritetut testit

[Gitlab Frontend](https://gitlab.labranet.jamk.fi/te-team1/conduit-frontend)
[Gitlab Frontend](https://gitlab.labranet.jamk.fi/te-team1/conduit-backend)
[TestLink Charts](https://195.148.22.11/testlink/lnl.php?apikey=1a3fab439652d5533067dc491074b5c151d0166be63f6eec5711106720e38973&tproject_id=782&tplan_id=1879&type=charts_basic)

## Muuta oleellista?

[Corpo Ration Corp. Kotisivu](http://te-team1.pages.labranet.jamk.fi/site/)

