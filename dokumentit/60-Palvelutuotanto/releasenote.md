# Release Note

## Sisällysluettelo

1. [Tuotenimi](#tuotenimi)
1. [Julkaisunumero](#julkaisunumero)
1. [Julkaisupäivämäärä](#julkaisupaivamaara)
1. [Release Note päivämäärä](#release_note_paivamaara)
1. [Release Note versio](#release_note_versio)
1. [Yleiskatsaus](#yleiskatsaus)
1. [Tarkoitus](#tarkoitus)
1. [NPM Audit Graph](#npm_audit_graph)
1. [NPM Audit Report Frontend](#npm_audit_report_frontend)
1. [NPM Audit Report Backend](#npm_audit_report_backend)
1. [Toteutetut featuret](#toteutetut_featuret)
1. [Feature Backlog](#feature_backlog)
1. [Suljetut User Storyt](#suljetut_user_storyt)
1. [Release noten laatija](#release_noten_laatija)
1. [Buglist](#buglist)
1. [Mahdolliset ratkaisut](#mahdolliset_ratkaisut)
1. [Vaikutukset käyttäjään](#vaikutukset_kayttajaan)

## Tuotenimi

*  WimmaForum versio 1.23 (kirahvi release)

## Julkaisunumero
 
* Versio 1.23
* kirahvi release
* conduit-frontend Digest: fa15c5b
* conduit-backend Digest: 1ff65d9

## Julkaisupäivämäärä

* 8.12.2020
* Loppuseminaari klo 12.00-13.00

## Release Note päivämäärä

* 4.12.2020

## Release Note versio

* versio 1.0

## Yleiskatsaus

WimmaForum on React + Node stackillä toteutettu RealWorld Conduit -foorumipohjalle rakennettu keskustelufoorumi. Tämä dokumentti kattaa kaikki palveluun lisätyt user storyt, featuret sekä bugilistauksen.

## Tarkoitus

Tämän dokumentin tarkoitus listata toteutettuja featureja sekä mahdollisia olemassaolevia ongelmia.

## NPM Audit Graph

[GoAccess Graph](http://te-team1.pages.labranet.jamk.fi/core/60-Palvelutuotanto/goaccess.html)

Oheisesta linkistä pääset katselemaan tietoturvakatselmointia graafisena.

## NPM Audit Report Frontend

```
NPM Audit Report
36
Known vulnerabilities

1,507
Dependencies

November 30th 2020, 6:18:13 am
Last updated

0
critical

2
high

2
moderate

32
low

0
info
``` 

## NPM Audit Report Backend 

```
NPM Audit Report
36
Known vulnerabilities

1,507
Dependencies

November 30th 2020, 6:18:13 am
Last updated

0
critical

2
high

2
moderate

32
low

0
info
```

## Toteutetut featuret

Tässä kaikki tuotantoon asti ajetut featuret.

| Ominaisuus |
|:-|
| [Feature 1 - GDPR](../20-Vaatimustenhallinta/Featuret/feature01-gdpr.md) |
| [Feature 3 - blacklist](../20-Vaatimustenhallinta/Featuret/feature03-blocklist.md) |
| [Feature 6 - palaute](../20-Vaatimustenhallinta/Featuret/feature06-palaute.md) |
| [Feature 7 - backendlog](../20-Vaatimustenhallinta/Featuret/feature07-backendlog.md) |
| [Feature 8 - googleanalytics](../20-Vaatimustenhallinta/Featuret/feature08-googleanalytics.md) |
| [Feature 9 - Docker](../20-Vaatimustenhallinta/Featuret/feature09-Docker.md) |
| [Feature 10 - HTTPS](../20-Vaatimustenhallinta/Featuret/feature10-TLS.md) |
| [Feature 11 - Branding](../20-Vaatimustenhallinta/Featuret/feature11-branding.md) |

## Feature backlog

Moderointi oli feature proposal. Feature 12 on julkaistu erillisenä branchina, mutta sitä ei ole yhdistetty master haaraan.

| Ominaisuus |
|:-|
| [Feature 2 - salasananpalautus](../20-Vaatimustenhallinta/Featuret/feature02-salasananpalautus.md) | 
| [Feature 4 - moderointipalvelu.md](../20-Vaatimustenhallinta/Featuret/feature04-moderointipalvelu.md) |
| [Feature 5 - käyttäjäroolit](../20-Vaatimustenhallinta/Featuret/feature05-kayttajaroolit.md) | 
| [Feature 12 - Some](../20-Vaatimustenhallinta/Featuret/feature12-some.md) |

## Suljetut User Storyt

<ul>
<li>US1 - Palvelun tuottajana haluan julkaista palvelun Docker-tekniikalla, koska se helpottaa tuotantoa</li>
<li>US2 - Testaajana haluan käyttää Docker-kontteja, koska se helpottaa testi ympäristön pystyttämistä</li>
<li>US3 - Kehittäjänä haluan käynnistää kehitysympäristön nopeasti käyttäen kontteja</li>
<li>US4 - Palvelu voidaan pystyttää ajamalla docker-compose up komento</li>
<li>US8 - Palvelun käyttäjän toivon, että palvelu on luotettava ja se käyttää suojattuja HTTPS-yhteyttä, koska en uskalla käyttää HTTP-palveluja nykyajassa
</li>
<li>US9 - Palvelun tuottajana haluan tietää nykyisen asiakaskunnan aktiivisen käyttöajan ja käyttäjä määrät viikon aikana, koska se selkeyttää ymmärrystä palvelun suosiosta</li>
<li>US10 - Palvelun tuottajana haluan tietää mitä selaimia asiakkaamme käyttävät, koska se selkeyttää ominaisuuksien kehittämistä palvelemaan loppuasiakasta paremmin</li>
<li>US11 - Palvelun tuottajana, haluan estää kirosanojen käyttämisen palvelussamme, koska haluan säästää moderointi-kuluissa</li>
<li>US12 - Palvelun tuottajana, haluan suodottaa suomenkieliset kirosanat, koska asiakaskuntamme nykytilanteessa on suomenkielinen 97 %</li>
<li>US13 - Palvleun tuottajana, haluan lisätä kieltolistaan muokkaamalla erillistä tiedostoa</li>
<li>US14 - Palvelun tuottajana haluan tulevaisuudessa voida käyttää kiellettyjen sanojen moderointiin ulkopuolista "sanitointi"-palvelua, koska se säästää moderointi-kuluja</li>
<li>US15 - Palvelun käyttäjän haluan, että keskustelu kanavalla on asiallista ja ei sisällä turhia kirosanoja</li>
<li>US18 - Palvelun tuottajana haluan saada palautetta loppukäyttäjilä, jonka perusteella tuotetta voidaan kehitää paremmaksi</li>
<li>US21 - Palvelun tuottajana meidän pitää kyetä tallentamaan aktiiviset tapahtumat palvelussa vähintään viimeisen viikon ajalta siten, että niitä voidaan tarkastella nopeasti (max 5 min)</li>
<li>US22 - Palvelun tuottajana haluan tallentaa viikon mittaiset käyttölogit erilliseen palvelimeen, koska palveluun murtautuja ei saa tuhota logeja</li>
<li>US23 - Palvelun tuottajana haluan, että login muoto on tulkittavissa silmämääräisti, mutta se sisältää aimmen esitetyt atribuutit</li>
<li>US25 - Palvelun tuottajana haluan suorittaa itsenäisen katselmoinnin palvelulle, koska palvelun ostaja haluaa saada nähtäväkseen mahdollisesti tietoturvaarvionnin
</li>
<li>US27 - Palvelun tuottajan on palvelustamme löydyttävä tietosuoja kuvaus, koska asiakkaamme sitä edellyttävät</li>
<li>US24 - Palvelun ylläpitäjänä haluan, että logit ovat koneluettavassa formaatissa jolloin niihin voidaan tehdä erilaisia hakuja ja parserointeja</li>
<li>US26 - Ohjelmistokehittäjänä haluan varmistua siitä ettei tuotteessa käytetä tunnettuja haavottuvia kirjastoja tai moduuleja</li>
<li>US31 - Järjestelmän ylläpitäjänä haluan että tuotannon tietokannasta otetaan säännölliset backup:it</li>
<li>US33 - Järjestelmän ylläpitäjänä vaadin että tietokanta on varmennettuna toisella koneella tai erillään tuotantopalvelimesta</li>
<li>US34 - Palvelun ylläpitäjänä haluan että palvelua tarjoillaan reverse proxyn takaa jolloin todelliset palvelimet piilotetaan sisäverkkoon</li>
<li>US40 - Palvelun tuottajana haluan saada kaiken Conduit-palvelun lähdekoodin hallituksi projekti group:in alla</li>
<li>US42 - Palvelun tuottajan haluan vaihtaa palvelun värit WIMMA Lab-sivuston mukaisiksi </li>
<li>US39 - Palvelun ylläpitäjänä haluan pystyä tekemään logitietoihin erilaisia hakuja ja visualisointeja</li>
<li>US41 - Palvelun tuottajana haluan käytää konttien jakamisessa labranetin gitlab rekisteriä </li>
<li>US43 - Palvelun tuottajana haluan, että palvelu huomauttaa artikkelin kirjoittajalle (suomeksi) tekstin sisällön asiallisuudesta, jos se sisältää "ala-arvoista" kieltä</li>
<li>US44 - Palvelun tuottajana haluan seurata palvelun käyttöastetta soveltaen apuna Google Analytics-palvelua, koska se on ollut aiemmin käytössä</li>
<li>US45 - Palvelun tuottajana haluan kerätä palvelun käyttöön liittyen käyttölogia, jonka perusteella voidaan selvittää mahdolliset väärinkäytökset</li>
<li>US47 Videoesittely</li>
<li>US49 TestLink PDF Testisuunnitelma</li>
<li>US52 TestChart / Passed-Failed</li>
<li>US63 Tuotepäällikkönä haluan tarjota asiakkaalle mahdollisuuden testata tuotetta ennen loppuseminaaria</li>
<li>US60 Palvelun tuottajana haluan varmistaa, että tärkeimmät toiminnallisuudet on testattu ja ne ovat asiakkaalle toimitettavissa</li>
<li>US61 Palvelun tuottajana haluan julkaista palvelusta "release note"-kuvauksen, josta löytyvät muutokset ja olemassa olevat viat</li>
<li>US64 Tuotepäällikkönä haluan antaa ulkopuolisille asiallisen kuvan projetin toiminnasta</li>
</ul>

## Buglist

* [Update your nick to 576 character. And it will break the layout.](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/155)
* [When write woo long tag and try to click it. It crash the site.](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/154)
* [Bad word filter won't react bad words if add some special mark in the front.](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/153)
* [Bug#9 - following when not logged in](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/135)
* [Bug#5 - Footer broken in article view](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/131)
* [Bug#4 - Liking an article when logged out](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/130)
* [Bug#3 - long words don't overflow right](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/127)

## Mahdolliset ratkaisut

* Pieniä muutoksia JavaScriptin puolelle Frontendissä.
* Production-versiossa osa poistuu itsekseen.

## Vaikutukset käyttäjään

* Bugien merkitys käytettävyyden osalta ei ole merkittävä.

## Release noten laatija

* Reima Parviainen