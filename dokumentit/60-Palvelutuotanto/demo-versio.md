# DEMO

## Mistä lötyy demo-palvelin?

* Demo-palvelun osoite: [https://corporation.devopskoulutus.net/](https://corporation.devopskoulutus.net/)

<iframe width="560" height="315" src="https://www.youtube.com/embed/MB022CtqAAU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## Anna palautetta?

<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSd4JtW5YqFGSCOyW3_I0nb6Y7yYGAhObAA7XpbYZpkRhynmJQ/viewform?embedded=true" width="640" height="1126" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>



