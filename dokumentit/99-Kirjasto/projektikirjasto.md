# Projektikirjasto

![](https://openclipart.org/image/300px/svg_to_png/268463/Library-no-text.png)

Alle on kerätty hyödyllisiä lähteitä, joista on apua projektien kanssa toimittaessa. Sisältö päivittyy ajoittain.



## Projektitoiminta yleisesti

* [Projektitoiminnan kurssin materiaalit](http://ttc2070.pages.labranet.jamk.fi/amk/20-projektihallinta/projektihallinta/)

## Laajempia tietoläheitä 

* [PMBOK](https://www.pmi.org/pmbok-guide-standards)
* [SWEBOK](https://www.computer.org/web/swebok/v3)
* [ISTQB](https://www.istqb.org/)
* []()

## Sanastot

* https://pascal.computer.org/sev_display/index.action
* [ISTQB-syllabus]()

## Ketteryys

* [Agile Essence](https://www.ivarjacobson.com/services/agile-essentials-starter-pack-agile-practices)
* [blog.crisp.se](https://blog.crisp.se/)

## Tietoturva

* https://www.suomidigi.fi/ohjeet-ja-tuki/tyokalut/turvallisen-sovelluskehityksen-kasikirja

## Canvas collection

* [Master Facilitator](http://masterfacilitator.com/canvas-collection/)
* [Team Charter Canvas](https://designabetterbusiness.com/2017/08/24/team-charter-canvas/)
* [Design Criteria Canvas](https://skillsofthemodernage.com.au/downloads/playshop/dabb-design-criteria-canvas.pdf)
