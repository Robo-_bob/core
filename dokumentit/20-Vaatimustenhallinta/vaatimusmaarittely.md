# Conduit-palvelun vaatimusmäärittely

### Toimeksianto   

* [Toimeksianto syksy 2020](http://ttos0800-ttos0900.pages.labranet.jamk.fi/amk-2020s/05-Toimeksianto/toimeksianto/)

### Tiimin jäsenten GitLab-tunnukset   

|  **Nimi**  | **GitLab-tunnus** |
| :----------: | :------: |
| **Reima** | [AA6135](https://gitlab.labranet.jamk.fi/AA6135) |
| **Sanni** | [AA6130](https://gitlab.labranet.jamk.fi/AA6130) |
| **Riku** | [P1384](https://gitlab.labranet.jamk.fi/P1384) |
| **Sami** | [AA6127](https://gitlab.labranet.jamk.fi/AA6127) |
| **Konsta** | [P1375](https://gitlab.labranet.jamk.fi/P1375) |
| **Sajid** | [AA0133](https://gitlab.labranet.jamk.fi/AA0133) |
| **Jukka** | [K0416](https://gitlab.labranet.jamk.fi/K0416) |
| **Khaled** | [AA6150](https://gitlab.labranet.jamk.fi/AA6150) |

### Dokumentin tiedot   

* Dokumentin versionumero 1.1   
* Vaatimusmäärittely pohjan versio 2.1 - 17.09.2020 (NarsuMan)

## Sisällysluettelo 

1. [Johdanto](#johdanto)
1. [Toimeksiantaja](#toimeksiantaja)
1. [Vaatimusmäärittelyn tekijä](#vaatimusmaarittelyn_tekijasta)
1. [Palvelukuvaus](#Palvelukuvaus)
1. [Sidosryhmäkartta](#sidosryhmakartta)
1. [Sidosryhmät ja profiilit](#sidosryhmat_ja_profiilit)
1. [Asiakkaan tarpeet/toiveet](#asiakkaan_tarpeettoiveet) 
1. [Tunnistetut riskit](#tunnistetut_riskit)
1. [Valitut asiakastarinat](#valitut_asiakastarinat)
1. [Palveluun liittyviä asiakaspolkuja](#palveluun_liittyvia_asiakaspolkuja)
1. [Oleelliset käyttötapaukset](#oleelliset_kayttotapaukset) 
1. [Tärkeimmät yleiset ominaisuudet/toiminnallisuudet](#tarkeimmat_ominaisuudettoiminnallisuudet) 
1. [MockUp-prototyyppi](#mockup-prototyyppi)
1. [Alustavat Käyttäjätarinat](#alustavat_kayttajatarinat)
1. [Palvelun järjestelmävaatimukset](#palvelun_liittyvat_tuotannolliset_ja_tekniset_vaatimukset) 
1. [Palveluun vaikuttavat rajaukset](#palvelun_toteuttamisen_kannalta_tarkeat_oleelliset_rajaukset_ja_standardit) 
1. [Palvelun liityvät laitevaatimukset](#palvelun_toiminnalliset_vaatimukset_functional_requirements)
1. [Palvelun määritellyt ominaisuudet eli featuret](#palvelun_ohjelmiston_ominaisuudet_eli_featuret)
1. [Palvelun toiminnalliset vaatimukset](#palvelun_toiminnalliset_vaatimukset_functional_requirements)
1. [Palvelun ei-toiminnalliset vaatimukset](#ohjelmistonpalveluun_ei-toiminnallisia_vaatimuksia)
1. [Palvelun alustava arkkitehtuuri](#ohjelmiston_arkkitehtuuri_sijoittelunakyma_tietokantakuvaus_ja_integraatiot)
1. [Palvelun alustava sijoittelunäkymä](#palvelun_sijoittelunakyma_deployment_diagram)
1. [Palvelun alustava tietokantakuvaus)](#tietokantakuvaus_database_er-diagram)
1. [Palvelun integraatiot muihin järjestelmiin](#integraatiot_muihin_jarjestelmiin)
1. [Palvelun laadun varmistuksesta](#palvelun_laadun_varmistus)
1. [Palvelun hyväksyntätestit](#palvelun_hyvaksyntatestit)
1. [Julkaisusuunnitelma](#julkaisusuunnitelma)gi

## Johdanto

>Projektissa työskennellään **WimmaLabin** tilaaman [Conduit](https://react-redux.realworld.io/#/?_k=b1awai)-palvelun parissa. Conduit-palvelun päätarkoitus olisi toimia Wimmalabin "foorumina", jossa voidaan julkaista kirjoituksia, joita muut käyttäjät voivat kommentoida ja mahdollisesti kehityksen myötä jakaa sosiaalisessa mediassa.   
Conduit-palvelussa voi kirjaantumatta selata muiden julkaisuja ja niiden kommentteja. Kirjautuminen palveluun tapahtuu käyttäjän omilla Conduit-tunnuksilla. Kirjautumisen jälkeen käyttäjä voi luoda oma julkaisuja sekä tykätä ja kommentoida muiden julkaisuja. Julkaisuihin voi myös lisätä tägejä.

## Toimeksiantaja

>**WimmaLab/Teemu** **Kontio** 

## Vaatimusmäärittelyn tekijästä

>[Corpo Ration Corp.](http://te-team1.pages.labranet.jamk.fi/site/)

Corpo Ration Corp. on syksyn 2020 Ohjelmistontuotanto ja laadunvarmistus sekä Moderni ohjelmistokehitys -opintototeutuksien alla toimiva opiskelijoiden muodostama projektiryhmä.

## Palvelukuvaus

>Conduit-palvelu toimii WimmaLabin foorumialustana, jossa voidaan luoda julkaisuja, joita muut voivat tykätä ja kommentoida. Käyttäjiä ovat JAMKin IT-istituutin opiskelijat ja henkilökunta. Conduitin tarkoituksena on olla alusta, jossa käyttäjäkunta voi jakaa tietoa ja luoda keskustelua.(???)   

## Sidosryhmäkartta

  ![Sidosryhmä](kuvat/sidosryhma01.png "Sidosryhmä")


## Sidosryhmät ja profiilit 


| Sidosryhmä/Profiili | Lisätietoa |
|:-|:-:|
| [Peruskäyttäjä](Profiilit/peruskayttaja.md) |WIMMA Lab:sta  Informaation jakaminen/kerääminen, verkostoituminen. |
| [WIMMA lab ryhmät](Profiilit/kehittynytkayttaja.md) | WIMMA Lab:sta  Informaation jakaminen/kerääminen, verkostoituminen. |
| [Asiakas - Teemu Kontio](Profiilit/aisiakas.md) | WIMMA Lab team coach, Haluaa kehittää WIMMA Lab:n kommunikointia |
| [Projektien ohjaus - Narsu Man](Profiilit/projektiohjaaja.md) | Projktin johto, WIMMA Lab omistaja, yhteys henkilö, ohjausta ja opettamista.   |
| Head coach - Hannu Oksman | Yhteys henkilö ja head of team coach  |
| [Kontakti - Mika Korhonen](Profiilit/kontakti.md) | Opastaa WIMMA Lab:n ryhmiä. Entinen WIMMA Lab:n jäsen  |
| [Alumnit](Profiilit/kontakti.md) | Entiset WIMMA Lab:n ryhmäläinen. Tarjoavat tukea turvaa nykyisillä WIMMA Lab:n ryhmäläisille. |
| [Yritys](Profiilit/yrityslahtoinenryhma.md) | Etsi mahdollisesti työntekijöitä.  |
| [Sponsorit](Profiilit/yrityslahtoinenryhma.md) | Antaa rahallista tukea, sekä etsii työntekijöitä. |
| [Moderaattori](Profiilit/tukihenkilo.md) | Foorumin tukihenkiö  |
| [Forum trolli](Profiilit/tuholainen.md) | Aiheuttaa yleistä mielipahaa ja sekasortoa.  |
| [Botit](Profiilit/tuholainen.md) | Levittää moinosttaa, levittää roskapostia ja palvelun estohyökkäys. |
| [Työntekijät Ryhmä 1-3](Profiilit/ryhma.md) | Pystyttää, kehittää ja testaa foorumit |
| [Yhteyhenkilö - Heli](Profiilit/yhteyshenkilo.md) | Projekti suunnittelu ja työnjako |


## Asiakkaan tarpeet/toiveet?

| VaatimusID | Tyyppi | Kuvaus | 								
|:-:|:-:|:-:|
| CUSTOMER-REQ-0001 | Customer Requirement | Käyttäjänä haluan jakaa muiden julkaisuja muille alustoille (esim. twitteriin) | 
| CUSTOMER-REQ-0002 | Customer Requirement | Käyttäjänä haluan löytää postaukset kategorioittain (voi toimiä myös tägeillä) |
| CUSTOMER-REQ-0003 | Customer Requirement | Etusivulla voisi olla erillinen "uutiset" osio |
| CUSTOMER-REQ-0004 | Customer Requirement | Käyttäjänä haluan kommentoida artikkeleita myös anonyymisti|


## Tunnistetut riskit

 * "Spambots"   
 * Valheellisen informaation levittäminen   
 * Muut keskustelufoorumit (miksi juuri Conduit?)   

## Valitut asiakastarinat

**Asiakas tarina Simo Räty**

[Peruskäyttäjä](liitteet/peruskayttaja.md)  koodailee omaa python Spagetti projektiaan. Ja törmää koodauksessa ongelmaan, jossa ei ole varma mikä olisi paras tapa toteuttaa se. Hän suuntaa kohti Googlea. Hän tykkää käyttää suomea hakukielenä, vaikka se ei ole optimaalinen etsintä tapa. Hän löytää projektin WIMMA Lab:n sivuilta projektin, joka arvio kuinka spagettia sinun kirjoittama koodisi on:(Projekti Ragetti Spagetti). Simo avaa viestiketjun. Ja hän käy viestiketjun läpi löytyykö hänen kysymykseensä vastausta. Ja ei näytä löytyvän. Hän tekee käyttäjä tunnuksen Wimma Lab:n foorumille. Hän esittää ongelmansa viestiketjuun ja poistuu foorumilta. Teemu Kontio huomaa viestin muutaman tunnin päästä. Hän kysyy projektin pääkoodaajalta Laura Liukkaalta pystyisikö hän vastamaan ongelmaan? Laura vastaa viestiketjuun seuraavana aamuna. Simo tulee katsomaan seuraavan viestiketjua seuraavana iltana. Ja hän esittää jatko kysymykseen, johonka saa vastauksen seuraavana aamuna. Simo on tyytyväinen, kun sai apua koodaus projektiinsa.   

**Asiakas tarina Simppa Häyhä**

[Simo](kehittynytkayttaja.md) toimii Projekti Ragetti Spagetin testaajana ja haluasi, että ohjelmoijat kommentoisivat koodiansa yhteisellä mallilla, kun häneltä menee paljon aikaa joidenkin ohjelmoijien koodin lukemisessa. Hän menee Wimma Lab:n foorumille ja tekee viestiketjun asiasta. Ja ehdottaa, että koodin kommentointi tyyli olisi yhtenäisempi ja ottaa esille Laura Liukkaan hyvät kommentointi tavat. Keskustelu alkaa ja kaikki eivät ole kovinkaan innokkaita muuttamaan koodaus tapojansa, mutta yhteinen sävel löytyy joka tapauksessa. Kommentointi on vieläkin jonkin verran puutteellista, mutta tämä on jossain määrin tyydyttävä tulos silti.  

**Asiakas tarina Teemu Kontio** 

 [Häneen](aisiakas.md) otetaan yhteyttä Karva-Kaken Koodaus Kabinetista, joka etsii innokkaita henkilöitä heidän projektiinsa. Ja jos projekti menee hyvin, olisi tiedossa madollinen työpaikka. Yritys tekee käyttäjätunnukset ja luo viestiketjun asiasta. Esittäytyy ja kertoo, että haluaisi tullaa esittäytymään ryhmille ja kertomaan projektista. Johonka Teemu vastaa, että kahden viikon päästä ma iltapäivällä olisi mahdollista tulla esittelemään yritystä ja projektia. Teemu pyytää lisätietoa projektista ja haluaa varmistaa, että tarvittavat tiedot kuten projektin laajuus, vaadittavat taidot ja aikataulut tulevat foorumille kirjalliseen muotoon. Yritys lisää vaaditut tiedot viestiketjuun. Teemu kertoo WIMMA lab:n ryhmille paikallisesti, että yritys esittely olisi sovittuna ajankohtana. Ja lisätietoja voi käydä katsomassa foorumin viestiketjusta.    


## Palveluun liittyviä asiakaspolkuja

**asiakaspolku PlantUML-esimerkki**

```plantuml
start
:Open WimmaLab Forum on web browser;
:Sing in;
:Sing up;
:Read the forum;
:Post stuff on forum;
:Favorite a few arcticles;
:Share some articles to your LinkedIn;
:Log out;
end
```

## Oleelliset käyttötapaukset

![](kuvat/usecases.png)


| Käyttötapaus | Osa-alue |
|:-:|:-:|
| [Käyttötapaus 1 - Artikkelin luonti](liitteet/kayttotapaus1.md) | Artikkelit |
| [Käyttötapaus 2 - Somejako](liitteet/kayttotapaus2.md) | Artikkelit |
| [Käyttötapaus 3.1 - Käyttäjätilin poisto](liitteet/kayttotapaus3.1.md) | Käyttäjänhallinta |
| [Käyttötapaus 3.2 - Käyttäjätilin poisto](liitteet/kayttotapaus3.2.md) | Käyttäjänhallinta |

## Tärkeimmät ominaisuudet/toiminnallisuudet 

![](kuvat/mindmap.png)   


## MockUp-prototyyppi

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Fproto%2FuISbBcn9myJVnNOmBtkb6h%2FWimmaLab-Forum%3Fnode-id%3D3%253A4%26viewport%3D735%252C944%252C0.6867609620094299%26scaling%3Dscale-down" allowfullscreen></iframe>

## Alustavat käyttäjätarinat

* [Palvelun tuottajana haluan juolkaista palvelun Docker-tekniikalla, koska se helpottaa tuotantoa](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/83)   
* [Testaajana haluan käyttää Docker-kontteja, koska se helpottaa testi ympäristön pystyttämistä](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/84)   
* [Testaajana haluan pystyttää uuden version Conduit-palvelusta alle minuutissa, koska testaajat ovat kiireisiä](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/85)   
* [WIMMA Lab-sidosryhmän edustajana toivon, että foorumille siirtyminen on helppoa ja selkeää](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/86)   
* [Toimeksiantajana toivon, että sivusto näyttää visuaalisesti yhtenäiseltä, jos käyttäjä menee foorumille](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/87)   
* [Forumin käyttäjänä toivon voivani palata helposti WIMMA Lab-sivustolle, koska haluan vielä tarkastella sitä uudelleen.](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/88)   
* [Palvelun käyttäjän toivon, että palvelu on luotettava ja se käyttää suojattuja HTTPS-yhteyttä, koska en uskalla käyttää HTTP-palveluja nykyajassa](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/89)   
* [Palvelun tuottajana haluan tietää nykyisen asiakaskunnan aktiivisen käyttöajan ja käyttäjä määrät viikon aikana, koska se selkeyttää ymmärrystä palvelun suosiosta](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/90)   
* [Palvelun tuottajana haluan tietää mitä selaimia asiakkaamme käyttävät, koska se selkeyttää ominaisuuksien kehittämistä palvelemaan loppuasiakasta paremmin](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/91)   
* [Palvelun tuottajana, haluan estää kirosanojen käyttämisen palvelussamme, koska haluan säästää moderointi-kuluissa](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/92)   
* [Palvelun tuottajana, haluan suodottaa suomenkieliset kirosanat, koska asiakaskuntamme nykytilanteessa on suomenkielinen 97 %](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/93)   
* [Palvleun tuottajana, haluan lisätä kieltolistaan muokkaamalla erillistä tiedostoa](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/94)   
* [Palvelun tuottajana haluan tulevaisuudessa voida käyttää kiellettyjen sanojen moderointiin ulkopuolista "sanitointi"-palvelua, koska se säästää moderointi-kuluja](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/95)   
* [Palvelun käyttäjän haluan, että keskustelu kanavalla on asiallista ja ei sisällä turhia kirosanoja](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/96)   
* [Palvelun käyttäjänä haluan tarvittavassa tilanteessa jakaa foorumin postauksen twitterin avulla ulkomaailmaan, koska joskus on hyvä tiedottaa ulkomaailmaa WIMMALABin tapahtumista.](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/97)   
* [Palvelun tuottajana haluan saada palautetta loppukäyttäjilä, jonka perusteella tuotetta voidaan kehitää paremmaksi](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/98)   
* [Palvelun tuottajana haluan auttaa loppukäyttäjää erillisen tukiportaalin kautta kuormittamatta kehitystiimiä ylimääräisillä kysmyksillä](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/99)   
* [Palvleun kehittäjän haluan saada loppukäyttäjän palautteen Issue-muodossa eteeni, koska se on selkeämpää jatko käsitellä](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/100)   
* [Palvelun tuottajana meidän pitää kyetä tallentamaan aktiiviset tapahtumat palvelussa vähintään viimeisen viikon ajalta siten, että niitä voidaan tarkastella nopeasti (max 5 min)](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/101)   
* [Palvelun tuottajana haluan tallentaa viikon mittaiset käyttölogit erilliseen palvelimeen, koska palveluun murtautuja ei saa tuhota logeja](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/102)   
* [Palvelun tuottajana haluan, että login muoto on tulkittavissa silmämääräisti, mutta se sisältää aimmen esitetyt atribuutit](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/103)   
* [Palvelun tuottajana haluan suorittaa itsenäisen katselmoinnin palvelulle, koska palvelun ostaja haluaa saada nähtäväkseen mahdollisesti tietoturvaarvionnin](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/104)   
* [Palvelun tuottajan on palvelustamme löydyttävä tietosuoja kuvaus, koska asiakkaamme sitä edellyttävät](https://gitlab.labranet.jamk.fi/te-team1/core/-/issues/105)   


## Palvelun liittyvät tuotannolliset ja tekniset vaatimukset 
 

| VaatimusID | Tyyppi | Kuvaus | Ominaisuus johon vaikuttaa |								
|:-|:-:|:-:|:-:|
| SYSTEM-HW-REQ-0002 | System Technical Requirement | Palvelun on toimittava CSC-palvelimella | Palvelun serveritoiminnot |
| SYSTEM-HW-REQ-0003 | System Technical Requirement | Palvelu tulee olla mahdollista pystyttää Docker compose up käskyllä  | Palvelun uudelleen pystytys |
| SYSTEM-HW-REQ-0004 | System Technical Requirement | Prosessorin arkkitehtuuri 64-bit, min. 4 ydintä käytössä | Palvelun toimintanopeus |
| SYSTEM-HW-REQ-0005 | System Technical Requirement | Palvelimen tulee kestää suuriakin liikennemääriä |Suorituskyky|

### Palvelun toteuttamisen kannalta tärkeät oleelliset rajaukset ja standardit

| Id | Vaatimuksen kuvaus | kategoria | Vastuullinen |
|:-:|:-:|:-:|:-:|
| CONSTRAINT-REQ-S00000 | Constrain | Käyttäjätietojen käsittelyn on noudatettava GDPR:n ohjeistusta  | [GDPR](Featuret/feature01-gdpr.md) [EU GDPR-säädös](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation)|
| CONSTRAINT-REQ-S00001 | Constrain | JHS Suositukset: JHS 190 Julkisten verkkopalvelujen suunnittelu ja kehittäminen | [JHS 190](http://docs.jhs-suositukset.fi/jhs-suositukset/JHS190/JHS190.html)


### Palvelun toiminnalliset vaatimukset (Functional Requirements)

| VaatimusID | Tyyppi | Kuvaus | Ominaisuus johon vaikuttaa |								
|:-|:-:|:-:|:-:|
| FUNCTIONAL-REQ-C0001 | Functional Requirement | Käyttäjänä haluan, että voin luoda oman tunnuksen foorumille | [Sign Up](Featuret Conduitissa/ogft01-signup.md) |
| FUNCTIONAL-REQ-C0002 | Functional Requirement | Käyttäjänä haluan kirjautua omalla tunnuksellani palveluun | [Kirjautuminen](Featuret Conduitissa/ogft02-signin.md) |
| FUNCTIONAL-REQ-C0003 | Functional Requirement | Omien käyttäjäasetuskien muokkaus|[Your Settings](Featuret Conduitissa/ogft03-yoursettings.md)|
| FUNCTIONAL-REQ-C0004 | Functional Requirement | Oman Profiilin muokkaaminen |[Profiili](Featuret Conduitissa/ogft04-profile.md)|

### Palvelun ohjelmiston ominaisuudet, eli "featuret"

* P1 = Pakollinen
* P2 = Tarpeellinen
* P3 = Tehdään, kun tarve ilmenee

| Ominaisuus | Prioriteetti | Use cases |
|:-|:-:|:-:|
| [Feature 1 - GDPR](Featuret/feature01-gdpr.md) | P1 | [GDPR](use_cases/gdpr_hyvaksyminen.md) |
| [Feature 2 - salasananpalautus](Featuret/feature02-salasananpalautus.md) | P2 |  [salasananpalautus](use_cases/salasanan_palautus.md) |
| [Feature 3 - blacklist](Featuret/feature03-blocklist.md) | P3 | [blocklist](use_cases/kiellettyjen_sanojen_estäminen.md) |
| [Feature 4 - moderointipalvelu.md](Featuret/feature04-moderointipalvelu.md) | P1 | [moderointi](use_cases/moderointi_viestin_poisto.md) |
| [Feature 5 - käyttäjäroolit](Featuret/feature05-kayttajaroolit.md) | P1 | [käyttäjäroolit](use_cases/antaa_moderointi_oikeudet.md) |
| [Feature 6 - palaute](Featuret/feature06-palaute.md) | P2 | [palaute](use_cases/palautteen_anto.md) |
| [Feature 7 - backendlog](Featuret/feature07-backendlog.md) | P2 | [backendlog](use_cases/backend_log_katsominen.md) |
| [Feature 8 - googleanalytics](Featuret/feature08-googleanalytics.md) | P3 |  |
| [Feature 9 - Docker](Featuret/feature09-Docker.md) | P1 | |
| [Feature 10 - HTTPS](Featuret/feature10-TLS.md) | P1 |  |
| [Feature 11 - Branding](Featuret/feature11-branding.md) | P1 |  |
| [Feature 12 - Some](Featuret/feature12-some.md) | P1 | [sosiaaliseen mediaan jakaminen](use_cases/sosiaaliseen_mediaan_jakaminen.md) |
| [OGFT01: Sign Up](Featuret Conduitissa/ogft01-signup.md) |  |  |
| [OGFT02: Sign In](Featuret Conduitissa/ogft02-signin.md) | |   |
| [OGFT03: Your Settings](Featuret Conduitissa/ogft03-yoursettings.md) |  | |
| [OGFT04: Profile](Featuret Conduitissa/ogft04-profile.md) | P1 | ) |
| [OGFT05: New Post](Featuret Conduitissa/ogft05-newpost.md) | P1 |  |
| [OGFT06: Feed / Home](Featuret Conduitissa/ogft06-feed.md) | P2 |  |
| [OGFT07: Tags](Featuret Conduitissa/ogft07-tags.md) | P2 |  |
| [OGFT08: Edit Article](Featuret Conduitissa/ogft08-edit.md) | P3 |  |
| [OGFT09: Delete Article](Featuret Conduitissa/ogft09-delete.md) | P1 | |
| [OGFT10: Post Comment](Featuret Conduitissa/ogft10-comment.md) | P1 |  |
| [OGFT11: Delete Comment](Featuret Conduitissa/ogft11-rmcomment.md) | P1 |  |
| [OGFT12: Follow User](Featuret Conduitissa/ogft12-follow.md) | P1 |  |
| [OGFT13: Unfollow User](Featuret Conduitissa/ogft13-unfollow.md) | P1 |  |
| [OGFT14: Favorite Article](Featuret Conduitissa/ogft14-favorite.md) | P1 |  |



### Ohjelmiston/palveluun ei-toiminnallisia vaatimuksia


| VaatimusID | Tyyppi | Kuvaus | Ominaisuus johon vaikuttaa |								
|:-|:-:|:-:|:-:|
| PERFORMANCE-REQ-0000 | Non-Functional Performance | Kirjautuminen on mahdollista yhtäaikaa 100 käyttäjällä (100 request/s) | [Kirjautuminen ft1](ft1-ominaisuus.md) |								

| VaatimusID | Tyyppi | Kuvaus | Ominaisuus johon vaikuttaa |								
|:-|:-:|:-:|:-:|
| SECURITY-REQ-0001 | Non-Functional Security | Palvelulla on käytettävä vähintään HTTPS salausta | [TLS/HTTPS](Featuret/feature10-TLS.md) |					
| SECURITY-REQ-0002 | Non-Functional Security | Palvelun on käsiteltävä käyttäjätietoja GDPR:n mukaisesti |[GDPR](Featuret/feature01-gdpr.md)|


| VaatimusID | Tyyppi | Kuvaus | Ominaisuus johon vaikuttaa |								
|:-|:-:|:-:|:-:|
| USABILITY-REQ-0000 | Non-Functional Usability | Palvelun pitää olla mutkatonta käyttää kirjautuneena tai ilman kirjautumista | [MockUp](#mock-up) | |	

| VaatimusID | Tyyppi | Kuvaus | Ominaisuus johon vaikuttaa |								
|:-|:-:|:-:|:-:|
| TESTABILITY-REQ-0000 | Non-Functional Testability | Palvelu on saatava pystytettyä nopeasti uudelleen Docker compose up komennolla  | [Docker](feature09-Docker.md)	 |	

## Ohjelmiston arkkitehtuuri, sijoittelunäkymä, tietokantakuvaus ja integraatiot



### Palvelun sijoittelunäkymä (Deployment diagram )

```plantuml
@startuml
actor User

cloud "Internet" as net{
queue "https"{
}
}

node "CSC srv1 / Ubuntu" as csc {
queue http {
}
node Docker {
node "Frontend" {
}
node "Backend" {
}
database "Mongo" {
}
}
card "Reverse Proxy / Apache" as rpa {
}
}
queue SSH {
}
node "CSC srv2 / Ubuntu" as csc2 {
database "database backup" as dbb {
}
database "sercive logs backup" as slb {
}
node "Backend" {
}
database "Mongo" {
}
}
User -- https
https -- rpa
rpa -- http
rpa -- http
Backend -- Mongo
http -- Backend
http -- Frontend
csc -- SSH
SSH -- slb
SSH -- dbb
@enduml
```

### Integraatiot muihin järjestelmiin

```plantuml
@startuml
card "Browser" as brow {
node "WimmaForum" as me {
}
}
node Google {
frame "Google Analytics" as goan {
}
}
node CSC-Server as csc {
node "Frontend" {
}
node "Backend" {
}
database "Mongo" {
}
}
node LinkedIn
node GitLab
node Doorbell.io
node "Social Media" as some {
node "Facebook" {
}
node "Twitter" {
}
node "Whatsapp" {
}
node "LinkedIn" {
}
me --> goan
me --> some
me --> csc
me --> GitLab
me --> Doorbell.io
GitLab --> me
csc --> me
Doorbell.io --> GitLab
@enduml
```

**Integraation kuvaaminen sekvenssikaaviona**


```plantuml
user ->server: Log in Request
server --> user : Authentication
user ->server: Post something
server --> user : Confirm
user ->server: Share a post
server --> user : Success
```

## Palvelun laadun varmistus

Testauksella varmistamme laadun.

## Palvelun hyväksyntätestit

| TestiID | Tyyppi | Kuvaus | Ominaisuus johon vaikuttaa |								
|:-:|:-:|:-:|:-:|
| USER-AT-0000 | User Acceptance Test | Palveluun pitää pystyä kirjautumaan | [Sign In](Featuret Conduitissa/ogft02-signin.md)	 |
| USER-AT-0001 | User Acceptance Test | Palvelun pitää toimia vakaasti ennen julkaisua | [Performance Testing Requirements](#ohjelmistonpalveluun_ei-toiminnallisia_vaatimuksia)|	
| USER-AT-0002 | User Acceptance Test ||	
| USER-AT-0003 | User Acceptance Test ||
| USER-AT-0004 | User Acceptance Test ||
| USER-AT-0005 | User Acceptance Test ||

| TestiID | Tyyppi | Kuvaus | Ominaisuus johon vaikuttaa |								
|:-:|:-:|:-:|:-:|
| OPER-AT-0000 | Operational Acceptance Test |  Palvelun pitää toimia salattuna ennen julkaisua | [TLS/HTTPS](Featuret/featuret10-TLS.md)	 |
| OPER-AT-0001 | Operational Acceptance Test | Palvelun pitää toimia vakaasti ennen julkaisua | [Performance Testing Requirements]
| OPER-AT-0002 | Operational Acceptance Test ||	
| OPER-AT-0003 | Operational Acceptance Test||	
| OPER-AT-0004 | Operational Acceptance Test ||	
| OPER-AT-0005 | Operational Acceptance Testy ||


## Julkaisusuunnitelma



```plantuml
Project starts the 2020-9-7
[Version v0.5 EarlyAdopter] Starts 2020-11-15 and ends 2020-12-11
[Design Phase] Starts 2020-7-9 and ends 2020-11-1
[Skeleton Conduit] Starts 2020-11-2 and ends 2020-11-8
[Skeleton Conduit with 30% of the features done.] Starts 2020-11-9 and ends 2020-11-15
[Skeleton Conduit with 60% of the features done.] Starts 2020-11-16 and ends 2020-11-22
[Meat puppet Conduit with 90% of the features done.] Starts 2020-11-23 and ends 2020-11-29
[Full Conduit with 100% of the features done.] Starts 2020-11-30 and ends 2020-12-6
[Accept testing] Starts 2020-12-7 and ends 2020-12-11
```



| Ominaisuus/toiminnallisuus | Versio | Milloin testattavissa | Julkaisu  |
|:-:|:-:|:-:|:-:|
| [Skeleton ](#mockup-prototyyppi) | 0.1 | 8.11.202 | V0.1 |
| [Skeleton Conduit 30%](#mockup-prototyyppi) | 0.3 | 15.11.2020  | V0.3 |
| [Skeleton Conduit 60%](#mockup-prototyyppi) | 0.6 | 22.11.2020 | V0.6 |
| [Meat Puppet Conduit 90%](#mockup-prototyyppi) | 0.9 | 29.11.2020 | V0.9 |
| [Full Conduit](#mockup-prototyyppi) | 1.0 | 13.12.2020 | V1.0 |



