## Tiivistetty tuotekuvaus:

> Tämä on tuotekuvaus pohja, joka kannattaa tiivistää A4-kokoon

* Palvelukuvaus

> Mikä palvelu on kyseessä ja ketä se palvelee?

## Liiketoiminnan tarpeet ja tavoitteet

* Tarpeet
* Tavoitteet

## Tärkeät sidosryhmät ja tuotteen kohderyhmä

* sidosryhmät?
* Kenelle tuote on tarkoitettu?

## Tärkeimmät hyödyt

* Sidoryhmä X hyötyy....

## Tärkeimmät toiminnallisuudet/ominaisuudet

> Mitkä ominaisuudet ja mille kohderyhmälle? Miten erotutaan markkinoista?

* Feature 1 + Feature 2 --> Kohderyhmä A ?
* Feature 3 + Feature 4 --> Kohderyhmä B ?
* Feature 5 + Feature 6 --> Kohderyhmä C ?

## Tekniset vaatimukset

> Mitä tarvitaan palvelun tuottamiseksi?

* System-Technical-Req 1
* System-Technical-Req 2
* System-Technical-Req 3
* System-Technical-Req 4