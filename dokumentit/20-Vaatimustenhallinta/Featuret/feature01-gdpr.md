# FT01: GDPR

Rekisteriseloste/GDPR

| | |
|:-:|:-:|
| Ominaisuus ID |FT01 |
| Osajärjestelmä, mihin ominaisuus liittyy | Tietoturva |
| Ominaisuuden vastuuhenkilö | - |
| Status | Keskeneräinen |

### Kuvaus

Feature käsittelee General Data Protection Regulationia (GDPR), eli EU:n henkilötietoja käsittelyä sääntelevää lakia. Projektiryhmä tarkastaa ja tuo esille, mitä tietoja Conduitin käyttäjästä kerätää, mihin niitä käytetään, ja miten ne voidaan poistaa (joko moden tai käyttäjän toiveesta).


### Ominaisuuteen liittyvät rajaukset, vaatimukset käyttötapaukset

*Kerätään tähän kaikki oleelliset asiat, jotka liittyvät ominaisuuden määrittelyyn tai osaltaan määrittävät sitä*

| | |
|:-:|:-:|
| [GDOR hyväksyminen](../use_cases/gdpr_hyvaksyminen.md) | |


### Alustavat käyttäjätarinat (User Storys)

> User Story
>"Haluan että sivuilla noudatetaan GDPR:n mukaista tietojenkäsittelyperiaatetta >ja haluan, että pystyn tutustumaan mitä tietoja minusta kerätään ja kehen otan >yhteyttä tietojeni käsittelyyn liittyen."


### Käyttöliittymänäkymä/mock 
Foorumille siirryttäessä landing page näkymässä GDPR seloste pomppaa silmille ja sitä pääsee tarkastelemaan klikkaamalla "Lue kokonaan" ja sen voi hyväksyä klikkaamalla paniketta "Hyväksyn tietojeni käyttämisen tällä sivustolla".

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FuISbBcn9myJVnNOmBtkb6h%2FWimmaLab-Forum%3Fnode-id%3D98%253A136" allowfullscreen></iframe>

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FuISbBcn9myJVnNOmBtkb6h%2FWimmaLab-Forum%3Fnode-id%3D98%253A158" allowfullscreen></iframe>






