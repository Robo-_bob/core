# FT11: Wimma Lab Branding


| | |
|:-:|:-:|
| Ominaisuus ID | FT11 |
| Osajärjestelmä, mihin ominaisuus liittyy | Ulkoasu |
| Ominaisuuden vastuuhenkilö | - |
| Status | Keskeneräinen |

### Kuvaus

Väriteemat / fontit vastaamaan WimmaLabin visuaalista ilmettä
WimmaLab logo näkyviin


Wimma Lab Forumin brandays tehdään Wimma Labin Black Bookin pohjalta. Ulkoasussa haetaan yhtenevää tyyliä Wimmalab.orgin kanssa.

WimmaLab.org etusivu
<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FuISbBcn9myJVnNOmBtkb6h%2FWimmaLab-Forum%3Fnode-id%3D98%253A128" allowfullscreen></iframe>

Wimma Lab Forumilla näkyvät kaikki Wimmalab.orgin tutut värin ja muotoilut. Fonttina samainen Open Sans sekä värit 333333 f4f4f9 575756 toistuvat Wimmalab.orgista tutussa kulmikkaassa ulkoasussa. 

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FuISbBcn9myJVnNOmBtkb6h%2FWimmaLab-Forum%3Fnode-id%3D3%253A4" allowfullscreen></iframe>


