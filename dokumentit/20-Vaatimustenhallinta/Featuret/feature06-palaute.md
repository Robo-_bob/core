# FT06: Palauteboxi

Doorbell.io-palauteboxi

| | |
|:-:|:-:|
| Ominaisuus ID | FT06 |
| Osajärjestelmä, mihin ominaisuus liittyy | Palaute |
| Ominaisuuden vastuuhenkilö | - |
| Status | Keskeneräinen |

### Kuvaus

Palauteboxin avulla saadaan käyttäjiltä kriittistä tietoa Conduitin toiminnasta ja parannusideoista. Palautteeseen hyödynnetään doorbell.io-sivuston tarjoamaa doorbelliä.

### Ominaisuuteen liittyvät rajaukset, vaatimukset käyttötapaukset

*Kerätään tähän kaikki oleelliset asiat, jotka liittyvät ominaisuuden määrittelyyn tai osaltaan määrittävät sitä*

| | |
|:-:|:-:|
| [Palautteen anto](../use_cases/palautteen_anto.md) | |




### Käyttöliittymänäkymä/mock 

Jokaisella sivulla alalaidassa näkyy door.io palvelun Feedback -painike. Painiketta klikkaamalla aukeaa palautelomake.

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FuISbBcn9myJVnNOmBtkb6h%2FWimmaLab-Forum%3Fnode-id%3D3%253A4" allowfullscreen></iframe>

Palautelomakkeeseen voi halutessaan laittaa oman sähköpostinsa, jos kaipaa suoraa vastausta. Alle ruutuun palaute.

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FuISbBcn9myJVnNOmBtkb6h%2FWimmaLab-Forum%3Fnode-id%3D42%253A174" allowfullscreen></iframe>







