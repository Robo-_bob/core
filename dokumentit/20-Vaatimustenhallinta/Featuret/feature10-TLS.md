# FT10: HTTPS Sertifikaatti


| | |
|:-:|:-:|
| Ominaisuus ID | FT10 |
| Osajärjestelmä, mihin ominaisuus liittyy | Tietoturva |
| Ominaisuuden vastuuhenkilö | - |
| Status | Keskeneräinen |

### Kuvaus

* Palvelun oletusportti 80 siirretään 443 porttiin
* Lisätään self signed-sertifikaatti
* Myös letsencrypt sertifikaatti mahdollinen (wimmalab luo domainin kun palvelimen IP osoite toimitetaan)




