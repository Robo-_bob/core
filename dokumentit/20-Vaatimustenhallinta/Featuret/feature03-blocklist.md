# FT03: Blocklist

Sanojen blocklist

| | |
|:-:|:-:|
| Ominaisuus ID |esim. FT03 |
| Osajärjestelmä, mihin ominaisuus liittyy | Artikkelin luonti/kommentointi |
| Ominaisuuden vastuuhenkilö | - |
| Status | Keskeneräinen |

### Kuvaus

Blocklistin avulla rajataan sanoja, joita käyttäjä voi käyttää luodessaan artikkelia tai kommentoidessaan sitä. Blocklist sisältää (näillä tiedoilla) rasistiset sekä loukkaavat sanat.


### Ominaisuuteen liittyvät rajaukset, vaatimukset käyttötapaukset


| | |
|:-:|:-:|
| [Kiellettyjen sanojen estäminen](../use_cases/kiellettyjen_sanojen_estaminen.md) | |




