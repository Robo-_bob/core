# FT09: Docker

Docker-compose up pystytys

| | |
|:-:|:-:|
| Ominaisuus ID | FT09 |
| Osajärjestelmä, mihin ominaisuus liittyy | Palvelun kontitus |
| Ominaisuuden vastuuhenkilö | - |
| Status | Keskeneräinen |

### Kuvaus

* Palvelu voidaan pystyttää ajamalla docker-compose up komento

* Testaajalle eduksi, että palvelusta voi pystyttää uuden version Conduit-palvelusta alle minuutissa, jotta testiympäristöjä on nopeampi pystyttää uudelleen ja uudelleen.





