# FT07: Backendlog

Backend logittaminen

| | |
|:-:|:-:|
| Ominaisuus ID | FT07 |
| Osajärjestelmä, mihin ominaisuus liittyy | Log |
| Ominaisuuden vastuuhenkilö | - |
| Status | Keskeneräinen |

### Kuvaus

Logissa päästään seuraavaan, mitä palvelun backend-järjestelmässä tapahtuu. Sieltä nähdään kellonaika ja tapahtuma, jonka kautta voidaan selvittää mahdollisia ongelmatilanteita. Huom! Koneluettava formaatti (esim. JSON)


### Ominaisuuteen liittyvät rajaukset, vaatimukset käyttötapaukset

*Kerätään tähän kaikki oleelliset asiat, jotka liittyvät ominaisuuden määrittelyyn tai osaltaan määrittävät sitä*

| | |
|:-:|:-:|
| [Backend log katselu](../use_cases/backend_log_katsominen.md) | |



