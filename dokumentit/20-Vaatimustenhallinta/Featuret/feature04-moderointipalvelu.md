# FT04: Moderointipalvelu

Moderointi

| | |
|:-:|:-:|
| Ominaisuus ID |esim. FT04 |
| Osajärjestelmä, mihin ominaisuus liittyy | Ylläpito |
| Ominaisuuden vastuuhenkilö | - |
| Status | Keskeneräinen |

### Kuvaus

Featuren kautta Conduit-palvelussa on erillisiä moderaattoreita, joilla on oikeudet poistaa artikkelejä, kommentteja sekä kokonaisia profiileja. Moderaattorille nostaa oikeudet toinen moderaattori. Moderoinnin hoitaa WimmaLab/moderointi ulkoistetaan.


### Ominaisuuteen liittyvät rajaukset, vaatimukset käyttötapaukset


| | |
|:-:|:-:|
| [Moderointi viestin poisto](../use_cases/moderointi_viestin_poisto.md) | |





