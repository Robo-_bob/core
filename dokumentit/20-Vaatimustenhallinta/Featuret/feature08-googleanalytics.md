# FT08: Google Analytics

Google Analytics

| | |
|:-:|:-:|
| Ominaisuus ID | FT08 |
| Osajärjestelmä, mihin ominaisuus liittyy | Käyttäjätiedot |
| Ominaisuuden vastuuhenkilö | - |
| Status | Keskeneräinen |

### Kuvaus

Käyttäjäanalytiikan avulla seurataan palvelun käyttäjien tietoja, esimerkiksi missä massa palvelua käytetään. Hyödynnetään tässä Google Analytics-palvelua.







