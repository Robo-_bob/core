# FT02: Salasanan palautus

Salasanan palauttaminen spostiin

| | |
|:-:|:-:|
| Ominaisuus ID |esim. FT02 |
| Osajärjestelmä, mihin ominaisuus liittyy | Käyttäjänhallinta |
| Ominaisuuden vastuuhenkilö | - |
| Status | Keskeneräinen |

### Kuvaus

Luodaan käyttäjälle mahdollisuus palauttaa unohdettu Conduit-salasana. Salasana palautetaan sähköpostiin, jota ollaan hyödynnetty käyttäjätilin luonnissa.


### Ominaisuuteen liittyvät rajaukset, vaatimukset käyttötapaukset

| | |
|:-:|:-:|
| [Salasanan palautus](../use_cases/salasanan_palautus.md) | |

### Alustavat käyttäjätarinat (User Storys)

> User story: "Jos unohdan salasanani, haluan päästä palauttamaan sen jotenkin. Olisipa jossain salasananpalautus toiminto."


### Käyttöliittymänäkymä/mock 

Salasanaa pääsee palauttamaan klikkaamalla Log In -sivulla "Forgot password?" -linkkiä. Tämä ohjaa salasananpalautussivulle.

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FuISbBcn9myJVnNOmBtkb6h%2FWimmaLab-Forum%3Fnode-id%3D7%253A3" allowfullscreen></iframe>

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FuISbBcn9myJVnNOmBtkb6h%2FWimmaLab-Forum%3Fnode-id%3D98%253A229" allowfullscreen></iframe>





