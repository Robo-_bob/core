# FT05: Käyttäjäroolit

Käyttäjäroolit - moderaattoriksi muuttaminen

| | |
|:-:|:-:|
| Ominaisuus ID |esim. FT05 |
| Osajärjestelmä, mihin ominaisuus liittyy | Ylläpito |
| Ominaisuuden vastuuhenkilö | - |
| Status | Keskeneräinen |

### Kuvaus

Moderaattori-oikeudet omaavalla täytyy olla mahdollisuus nostaa toinen käyttäjä moderaattoriksi. 

### Ominaisuuteen liittyvät rajaukset, vaatimukset käyttötapaukset

*Kerätään tähän kaikki oleelliset asiat, jotka liittyvät ominaisuuden määrittelyyn tai osaltaan määrittävät sitä*

| | |
|:-:|:-:|
| [Antaa moderointi oikeudet](../use_cases/antaa_moderointi_oikeudet.md) | |






