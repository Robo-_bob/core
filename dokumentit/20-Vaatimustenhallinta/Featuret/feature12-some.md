# FT12: Sosiaaliseen mediaan jakaminen


| | |
|:-:|:-:|
| Ominaisuus ID | FT12 |
| Osajärjestelmä, mihin ominaisuus liittyy | Käyttäjätoiminnot |
| Ominaisuuden vastuuhenkilö | - |
| Status | Keskeneräinen |

### Kuvaus

Jaa twitteriin / facebook tms. painike postauksiin



### Ominaisuuteen liittyvät rajaukset, vaatimukset käyttötapaukset

*Kerätään tähän kaikki oleelliset asiat, jotka liittyvät ominaisuuden määrittelyyn tai osaltaan määrittävät sitä*

| | |
|:-:|:-:|
| [Sosiaalisen median jakaminen](../use_cases/sosiaaliseen_mediaan_jakaminen.md) | |

### Alustavat käyttäjätarinat (User Storys)


Palvelun käyttäjänä haluan tarvittavassa tilanteessa jakaa foorumin postauksen twitterin avulla ulkomaailmaan, koska joskus on hyvä tiedottaa ulkomaailmaa WIMMALABin tapahtumista.



### Käyttöliittymänäkymä/mock 
Käyttäjä voi jakaa artikkelin haluamaansa sosiaalisen median palveluun klikkaamalla jaa -painiketta.

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FuISbBcn9myJVnNOmBtkb6h%2FWimmaLab-Forum%3Fnode-id%3D7%253A46" allowfullscreen></iframe>

Käyttäjä voi kirjoittaa viestikenttään kommenttinsa artikkelista. 

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FuISbBcn9myJVnNOmBtkb6h%2FWimmaLab-Forum%3Fnode-id%3D147%253A183" allowfullscreen></iframe>




