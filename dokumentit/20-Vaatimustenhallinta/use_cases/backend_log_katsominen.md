### Backend log seuraus

| Valinta | Kuvas |
| ------ | ----------- |
| Use case: | Backend log seuraus|
| Käyttäjä: | Moderaattori|
| Seloste: | Kirjautumisen jälkeen näet oikeassa yläkulmassa. Samalla rivillä kun "asetukset" löytyy linkki ""Backend log". Klikkaa "Backend log" linkkiä. Tämän jälkeen tulee näkymä, jossa näkyy järjestelemän viimeisin tapahtuma mikä näkyy ensimmäisenä, kun haluan nähdä vanhempia logeja selaan listaa alaspäin. Ja voin selata listaa järjestelmän alku aikoihin saakka. |
| Esiehto: | Käyttäjä on kirjautunut admin tunnuksilla. |