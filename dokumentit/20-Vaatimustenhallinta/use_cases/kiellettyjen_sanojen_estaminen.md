### Kiellyttejen sanojen estäminen

| Valinta | Kuvas |
| ------ | ----------- |
| Use case: | Kiellettyjen sanojen estäminen |
| Käyttäjä: | peruskäyttäjä|
| Seloste: | Kun kirjoitan viestiäni. Ja viestissäni on kielletty sana. Ja yritän julkaista viestin. Järjestelmä ilmoittaa keskelle ruutua, että "Viestissäsi on kiellettyjä sanoja. Ole hyvä ja tarkista viestisi sisältö. Kiitos" |
| Esiehto: | OGFT05 < Olet kirjoittamassa uutta viestiä. |
