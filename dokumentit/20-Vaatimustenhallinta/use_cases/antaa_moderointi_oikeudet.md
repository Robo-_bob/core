### Antaa moderointi oikeudet

| Valinta | Kuvas |
| ------ | ----------- |
| Use case: | Antaa moderointi oikeudet. |
| Käyttäjä: | Moderaattori|
| Seloste: | Käyttäjä on selaamassa foorumin viestejä. Ja klikkaa käyttäjän nimeä. Follow painikkeen vasemmalla puolella on nappula "Anna admin oikeudet "Tämän jälkeen järjestelmän antaa pop-up keskelle näyttöä, jossa lukee "Oletko varma?". Johon painetaan "Ok" nappulaa pop-up. |
| Esiehto: | OGFT02 kirjautunut moderaattori oikeuksilla. |