### GDPR Hyväksyminen 

| Valinta | Kuvas |
| ------ | ----------- |
| Use case:  | GDPR hyväksyminen   |
| Käyttäjä: | peruskäyttäjä|
| Seloste:     | Käyttäjä avaa foorumin sivut. Milloinka GDPR pop-up ikkuna avautuu. Sivun keskelle. Missä lukee GDPR ehdot. Hän hyväksyä ehdot. Painamalla nappulasta "Hyväksyn tietojeni käyttämisen tällä sivulla". |
| Esiehto: | Käyttäjä on aukaissut verkkoselaimen.|
