### Salasanan palautus 

| Valinta | Kuvas |
| ------ | ----------- |
| Use case: | Salasanan palautus |
| Käyttäjä: | peruskäyttäjä|
| Seloste: | Käyttäjä avaa foorumin sivut. Klikkaa oikeassa reunassa olevaa painiketta "Login". Tämän jälkeen klikkaa linkkiä "Forgot password?". Syötä palautus sähköposti osoitteesi valkoiseen laatikkoon. Ja paina nappulaa "Send pasword reset link". |
| Esiehto: | Käyttäjä on aukaissut verkkoselaimen. |
