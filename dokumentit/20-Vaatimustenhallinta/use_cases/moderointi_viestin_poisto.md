### Moderointi viestin poisto

| Valinta | Kuvas |
| ------ | ----------- |
| Use case: | Poista muiden tekemiä viestejä. |
| Käyttäjä: | Moderaattori|
| Seloste: | Käyttäjä on selaamassa foorumin viestejä. Ja klikkaa viestin otsikkoa minkä haluat poistaa. Ja tämän jälkeen käyttäjä klikkaa "Delete Article" nappulaa. |
| Esiehto: | OGFT02 kirjautunut  |