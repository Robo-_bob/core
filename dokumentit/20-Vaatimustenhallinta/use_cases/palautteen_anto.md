### Palautteen anto

| Valinta | Kuvas |
| ------ | ----------- |
| Use case: | Palautteen anto|
| Käyttäjä: | Peruskäyttäjä|
| Seloste: | Klikkaa oikeassa alareunassa olevaa "Feedback" nappulaa. Anna sähköpostisi kohtaan "Your email(optional), jos haluat. Kirjoita palautteesi "Your feedback" osioon. Ja klikkaa "Submit" nappulaa. |
| Esiehto: | Käyttäjä on foorumin sivuilla|
