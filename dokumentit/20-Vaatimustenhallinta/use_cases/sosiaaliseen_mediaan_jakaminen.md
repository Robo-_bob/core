### Sosiaaliseen mediaan jakaminen

| Valinta | Kuvas |
| ------ | ----------- |
| Use case: | Sosiaaliseen mediaan jakaminen|
| Käyttäjä: | Perukäyttäjä|
| Seloste: | Kun selaat viestejä. Artikkelin otsikon oikealta puolelta löytyy pieni kuva twitteriin ja Facebook:n. Joita klikkaamalla voit jakaa artikkeelin. Mieleisellesi sosiaaliseen mediaan |
| Esiehto: | Olet foorum sivustolla |