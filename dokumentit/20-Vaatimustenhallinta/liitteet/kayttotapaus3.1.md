# Käyttötapaus:

Käyttäjätilin poisto (käyttäjän näkökulmasta)

## Use Case 


```plantuml
Kayttaja -- (Tilin poisto)
Mode -- (Tilin poisto)
```

* Laatija: AA6130
* 12.10.2020
	
**Käyttäjäroolit**	

1. Käyttäjä (esim. [peruskäyttäjä](peruskayttaja.md)

**Esitiedot/ehdot**	

1. Käyttäjällä on ennestään luotu käyttäjätili
2. Käyttäjä on kirjautunut sisään

**Käyttötapauksen kuvaus**

1. Käyttäjä painaa oikeassa yläkulmassa sijaitsevaa "Settings"
2. Asetukset avautuvat, käyttäjä rullaa asetusten loppuun
3. Asetusten alla sijaitsee nappi, jossa lukee punaisella pohjalla "Delete my account"
4. Klikkaamalla nappia ruudulle ilmestyy popup, jossa  pyydetään selitystä tilin poistolle (optional). Popupissa on myös 
* "I want all my data removed with my account" Tämä checkboxin merkattuaan käyttäjä haluaa hävittää kaikki tietonsa Conduitista (joka tästä apremmin selvillä saa selventää)
* "I am sure I want to delete my account" Pakollinen checkbox käyttäjätilin poistoon
5. Accept-napin painallus kirjaa käyttäjän automaattisesti pois, ja ilmoittaa mahdollisesta ajasta, jonka sisällä kaikki käyttäjätiedot ovat poistettu, eikä käyttäjätunus enää ole käytössä.

**Poikkeukset**
 
	
**Lopputulos**	

Samalla käyttäjätunnuksella ei pääse enää kirjautumaan Conduit-palveluun, ja kaikki käyttäjän tiedot tullaan hävittämään, jos ensimmäinen checkbox on merkattu.

**Käyttötiheys** 

Ei kovin usein

**Muuta**	

Moderaattori pystyy myös poistamaan käyttäjän tilin, jos hän kokee käyttäjästä harmia palvelulle. Tästä lisää [täällä](kayttotapaus3.2.md)



**Lähteet**

Tämä wiki-dokumentin runko pohjautuu [Julkisenhallinnon suosituksiin](http://www.jhs-suositukset.fi/web/guest/jhs/recommendations/173)

Kiitokset alkuperäisen tekijöille!

