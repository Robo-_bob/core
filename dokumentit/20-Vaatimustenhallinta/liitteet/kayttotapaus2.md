# Käyttötapaus:

Artikelin jakaminen toiselle sosiaalisen median alustalle

## Use Case 

* Laatija: AA6130
* 12.10.2020

	
**Käyttäjäroolit**	

1. Käyttäjä (esim. [peruskäyttäjä](peruskayttaja.md)

**Esitiedot/ehdot**	

1. Käyttäjällä ei tarvitse olla käyttäjätunnusta artikkelin jakamiseen
2. Käyttäjä on avannut artikkelinäkymän klikkaamalla haluamaansa artikkelia

**Käyttötapauksen kuvaus**

1. Käyttäjä klikkaa artikkelin otsikon oikealla puolella sijaitsevaa "Share this article"-nappia
2. Näytölle ilmestyy alavalikko, jossa on eri some-alustojen logoja (Facebook, Twitter yms)
3. Klikkaamalla logoa ruudulle tulee näkyviin esim. Facebookin popup, joka ohjaa käyttäjää jakamisen loppuun.
4. Onnistunnen jakamisen jälkeen käyttäjä jää normaaliin artikelinäkymään

**Poikkeukset**
 
	
**Lopputulos**	

Käyttäjä on onnistuneesti jakanut artikkelin linkin toiselle alustalle. Artikkelin share-counter nousee(optional)

**Käyttötiheys** 

Käyttötapausta suoritetaan kohtalaisen usein.

**Muuta**	





**Lähteet**

Tämä wiki-dokumentin runko pohjautuu [Julkisenhallinnon suosituksiin](http://www.jhs-suositukset.fi/web/guest/jhs/recommendations/173)

Kiitokset alkuperäisen tekijöille!

