# Käyttötapaus:

Käyttäjätilin poisto (moderaattori)

## Use Case 


* Laatija: AA6130
* 12.10.2020
	
**Käyttäjäroolit**	

1. Moderaattori
2. Tilin haltija

**Esitiedot/ehdot**	

1. Moderaattorilla on käyttäjätunnukset, ja hänellä on moderaattorin oikeudet foorumilla
2. Poistettava käyttäjä on aiheuttanut harmia foorumille, kuten levittänyt haitallista sisältöä tai käytäytynyt häiriköivästi
3. Moderaattori on kirjautunut sisään ja avannut poistettavan käyttäjän käyttäjänäkymän

**Käyttötapauksen kuvaus**

1. Moderaattori klikkaa oikealla "follow (username)" alapuolella sijaitsevaa punaisella pohjalla lukevaa "Delete this user"
2. Popup kysyy moderaattorilta tilin poistamisen syytä (moderaattorin on hyvä lisätä selvitys, jotta muut modet ymmärtävät miksi käyttäjä on poistettu) ja checkbox "I am sure I want to delete this account" (pakollinen poiston onnistumiseksi)
3. Kun moderaattori on hyväksynyt käyttäjän poiston, siitä tulee automaattinen ilmoitus käyttäjän sähköpostiin, jossa hänelle kerrotaan, että tili on poistettu ja käyttäjätunnus ei ole enää käytössä 
4. Sähköpostissa on myös yhteysteidot, johon käyttäjä voi tehdä valituksen, jos hän uskoo, että hänet on poistettu foorumilta syyttä


**Poikkeukset**
 
	
**Lopputulos**	

Käyttäjätunnuksella ei pääse enää kirjautumaan Conduit-palveluun

**Käyttötiheys** 

Ei käytetä usein (ainakaan toivottavasti)

**Muuta**	




**Lähteet**

Tämä wiki-dokumentin runko pohjautuu [Julkisenhallinnon suosituksiin](http://www.jhs-suositukset.fi/web/guest/jhs/recommendations/173)

Kiitokset alkuperäisen tekijöille!

