# Käyttötapaus:

Artikkelin luonti


## Use Case 

```plantuml
Kayttaja -- (Artikkelin luonti)
```


* Laatija: AA6130
* 12.10.2020
	
**Käyttäjäroolit**	

1. Käyttäjä (esim. [peruskäyttäjä](peruskayttaja.md))

**Esitiedot/ehdot**	

1. Käyttäjällä on ennestään luotu käyttäjätunnus   
2. Käyttäjä on kirjautunut sisään

**Käyttötapauksen kuvaus**

1. Conduitin etusivulla käyttäjä painaa oikeassa yläkulmassa sijaitsevaa "New Post"
2. Käyttäjä syöttää kenttiin artikkelin otsikon, aiheen, itse artikkelin (markdownissa) ja tagit
3. Käyttäjä painaa vihreää paniketta "Publish Article"
4. Conduit vie käyttäjän artikkelinäkymään, jossa käyttäjä voi lisätä kommentteja tai poistaa tai muokata artikkelia


**Poikkeukset**
 
	
**Lopputulos**	

Suorituksen jälkeen artikkeli näkyy seuraavissa paikoissa:
 * Etusivu - Global Feed   
 * (Käyttäjätunnus oikeassa ylänurkassa) - My Articles   

**Käyttötiheys** 

Riippuu käyttäjästä, kuinka usein käyttäjä luo artikkeleita. Yleisellä tasolla tapahtumaa suoritetaan paljon.   

**Muuta**	

Muut käyttäjät voivat jälkikäteen kommentoida artikkelia, lukea sitä, sekä lisätä sen lempiartikkeleihinsa.



**Lähteet**

Tämä wiki-dokumentin runko pohjautuu [Julkisenhallinnon suosituksiin](http://www.jhs-suositukset.fi/web/guest/jhs/recommendations/173)

Kiitokset alkuperäisen tekijöille!

