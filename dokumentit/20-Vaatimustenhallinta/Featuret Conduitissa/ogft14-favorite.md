# OGFT14: Favorite Article

Favorite Arcticle Feature - Käyttäjä voi tykätä artikkeleista painamalla <3 painiketta artikkelin vieressä. 

| | |
|:-:|:-:|
| Ominaisuus ID |OGFT14 |
| Osajärjestelmä, mihin ominaisuus liittyy | Käyttäjätoiminnot |
| Ominaisuuden vastuuhenkilö | - |
| Status | Valmis |

### Kuvaus

Käyttäjä voi tykätä artikkelista klikkaamalla <3 painiketta. Painike näyttää montako tykkäystä kyseisellä artikkelilla on.

### Ominaisuuteen liittyvät rajaukset, vaatimukset käyttötapaukset

*Kerätään tähän kaikki oleelliset asiat, jotka liittyvät ominaisuuden määrittelyyn tai osaltaan määrittävät sitä*

| | |
|:-:|:-:|
| [Use Case 1](FT1-kayttotapaus.md) | |
| [Use Case 2](FT2-kayttotapaus.md) | |
| [Vaatimus ReqID]() |  | 
| [Vaatimus ReqID]() |  | 
| [Vaatimus ReqID]() |  | 

### Alustavat käyttäjätarinat (User Storys)

*Kirjataan User Storyt alustavasti tähän*

* Käyttäjän haluan, että voin.....
* Ylläpitäjänä haluan, että voin.....

**Nämä kannattaa siirtää pian issue kuvauksiksi*

esim. #8 #2


### Käyttöliittymänäkymä/mock 

> Tähän kannattaa liittää tarvittaessa kuvausta kuvan/mockup-näkymän muodossa. 
Se helpottaa ymmärtämään tarvittaessa oleellisesti ominaisuutta/toiminnallisuutta

```plantuml
salt
{
  Just plain text
  [This is my button]
  ()  Unchecked radio
  (X) Checked radio
  []  Unchecked box
  [X] Checked box
  "Enter text here   "
  ^This is a droplist^
}
```


### Testaus / mahdolliset hyväksyntä kriteerit 

*Kirjataan muutamia huomiota testauksen kannalta*

| Testitapaus  | Testin lähde  | Kuka vastaa  |
|:-: | :-:|:-:|
| [Hyväksyntätesti 1](pohjat/pohja-hyvaksyntatesti.md)  | vaatimus id?   |   |
| [Hyväksyntätesti 2](pohjat/pohja-hyvaksyntatesti.md)  | vaatimus id?   |   |
| [Hyväksyntätesti 3](pohjat/pohja-hyvaksyntatesti.md)  | vaatimus id?   |   |
| [Hyväksyntätesti 4](pohjat/pohja-hyvaksyntatesti.md)  | vaatimus id?   |   |
| | |





