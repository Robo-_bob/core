# Profiili: Yrityslähtöinen ryhmä 


## Viiteryhmä/segmentti:

 
Yritykset, sponsorit

## Persoonan kuvaus

* Karva-Kaken Koodaus Kabinetti
* Yritys

## Motiivi käyttää/soveltaa palvelua?

Etsii työvoimaa ja mahdollisiset haluaa sponsoiroida, jos saavat näkyvyyttä yritykselle. Haluaa tiedustella tarkemmin foorimia ja tarkastella minkälaista väkejä WIMMA lab:sta löytyy. 

## Arvot

* Haluetaa löytää hyvä ryhmätyöntekijä. 
* Yrityksen tuotto hyvin tärkeä.
* Itsensä mainostaminen hyvin tärkeä.


## Välineet ja kyvyt etc.
* Pieni yritys
* Valmis pieni asiakas pohja.
* Jonkin verran pääomaa.
