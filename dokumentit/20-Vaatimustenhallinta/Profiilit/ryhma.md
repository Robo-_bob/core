# Profiili: Työryhmäläinen


## Viiteryhmä/segmentti:

 
Projekti työläinen

## Persoonan kuvaus

* Sami
* 37v

## Motiivi käyttää/soveltaa palvelua?

Halua testata järjestelmän ja sen ominaisuudet. Siihen pisteeseen että sitä kehtaa esitellä ihmisille.

## Arvot

* Pitää it-alasta.
* Haluaa saada projektin päätökseen siedettävässä kunnossa.
* Lojaali ryhmälle


## Välineet ja kyvyt etc.
* Perus ymmärrys testauksesta.
* Kokemusta Robot Frameworks:sta
* Pystyy tekemään ryhmässä projektia

