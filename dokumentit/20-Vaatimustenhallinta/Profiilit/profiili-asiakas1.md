# Asiakasprofiili


## Viiteryhmä/segmentti:

_Asiakas_   
IT-instituutin opiskelija   


## Persoonan kuvaus

* Sami Rantalainen
* 22-vuotias
* JAMK IT-instituutin opiskelija, 3. vuosikurssi

## Motiivi käyttää/soveltaa palvelua?

Saada ajankohtaista tietoa niin WimmaLabista, kuin muista IT-instituuttia koskevista asioista. Haluaa myös käyttää palvelua keskustelemiseen.   

## Arvot

* Palvelun monipuolinen käyttö
* 


## Välineet ja kyvyt etc.

Omistaa todennäköisesti tunnukset palveluun, jolloin Samilla enemmän ominaisuuksia käytettävissä.
