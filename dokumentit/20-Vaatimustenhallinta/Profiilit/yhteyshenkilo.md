# Profiili: Yhteyshenkilö


## Viiteryhmä/segmentti:

 
Scrum Master

## Persoonan kuvaus

* Heli
* Jamk opinto-ohjaaja

## Motiivi käyttää/soveltaa palvelua?

Tukee Narsu Man projektin suunnittelua. Antaa tukea turvaa työryhmäläisille.

## Arvot

* Projekti menee eteenpäin.
* Haluaa että ryhmien kommunikointi ja tiedottaminen toimii.
* Ryhmien hyvin vointi.


## Välineet ja kyvyt etc.
* It-insinööri kurssit.
* Hyvät kommunikointi taidot.
* Järjestelmällisyys.

