# Profiili: Projekti ohjaaja


## Viiteryhmä/segmentti:

 WIMMA lab tuotteen omistaja ja projekti ohjaaja



## Persoonan kuvaus

* Marko Rintamäki(Narsu Man)
* Projekti johtajat, tuotteen omistaja, team couch

## Motiivi käyttää/soveltaa palvelua?

Haluaa WIMMA Lab:n paremman tavan kommunikoida ihmisten kanssa, kuin sähköposti tai Linked in. Toimii tässä tapauksessa välikätenä asiakaan ja ryhmän välillä. Opettaa ja ohjaa ryhmäläisiä projektissa. Toimii projektin ulkopuolella yhteys henkilönä WIMMA lab:n. 

## Arvot

* Kommunikointi kaikki kaikessa.
* Työlähtöinen opettamis tapa.
* Opettamisen innovointi.


## Välineet ja kyvyt etc.
* On hyvin kyvykäs jamk opettaja.
* Runsas kokemus testaus alalta.
* Hyvät kommunikointi taidot.

