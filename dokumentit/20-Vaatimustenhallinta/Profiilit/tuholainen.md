# Profiili: Tuholainen


## Viiteryhmä/segmentti:

 
botti, trolli

## Persoonan kuvaus

* Timo Tuomio
* 38v


## Motiivi käyttää/soveltaa palvelua?

Hän haluaa testailla ihmisiä ja järjestelemiä. Mikä on heidän rajansa. Ja tykkää ajella rajan yli uudelleen ja uudelleen, kun hän saa jotain rikki. Hän pönkittää itsetuntoansa sillä. 


## Arvot

* Narsistinen 
* Toimiva sosiopaatti.
* Eristäytynyt yhteyskunnasta

## Välineet ja kyvyt etc.
* scriptaus ja automatisointi
* terävä kieli 
* Kokemusta foorumin manipuloinnista.

