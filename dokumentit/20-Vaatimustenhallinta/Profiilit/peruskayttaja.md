# Profiili: Peruskäyttäjä


## Viiteryhmä/segmentti:

 Foorumin käytäjä   



## Persoonan kuvaus

* Simo Räty
* 20-vuotias
* Vaalmistunut lukiosta.

## Motiivi käyttää/soveltaa palvelua?

Töitä hän yritti löytää, mutta työmarkkinat ovat hyvin vaativat. Useammasta paikasta todettiin. Ei ole tarpeeksi kokemusta ja koulutusta. 

Hän jäi ilman korkekoulu paikkaa, koska hakumäärä oli niin suuri ja koe haastava. Kela on pyöräyttänyt hänel pitkän karenssin. Lukiosta valmistumisen jälkeen. Ja hän voi käytännössä saada tukea Kelalta, jos löytää harjoittelu paikkaann.

Hän etsii ajankohtaista tietoa niin WimmaLabista, kuin muista IT-instituuttia koskevista asioista. Haluaa myös käyttää palvelua keskustelemiseen. Halua tiedustella minkälainen paikka on, ennen kuin hakee harjoittlu paikkaa.

## Arvot

* Hän tykkää hajoittaa asioita. Mielenkiinnota pystyykö nostamaan ne takaisin tomintaan.
* Tykkkää tietokoneiden lisäksi kaikkesta teknisetä härpäkkeistä kuten(Dronet,automatisoidut laitteet,)
* Pitää aseista. Harrastaa airsof:ta. Pitää sotilas simulaattoreista kuten (Arma 3). Tekee omia kampanjoita Arma 3. Joita pelailee kavereidensa kanssa. 


## Välineet ja kyvyt etc.
* Hän omistaa läppärin. 
* Hän on saanut opetusta Javascrit lukiosta
* On koodannut muutaman pienen aplikaation  
* Ja tehnyt muutman automatio scriptin koneellensa.


