# Profiili: Kehittynyt käyttäjä


## Viiteryhmä/segmentti:

 WIMMA lab ryhmäläinen



## Persoonan kuvaus

* Simppa Häyhä
* 22-vuotias
* Kolmannen vuoden opiskelija Jamk

## Motiivi käyttää/soveltaa palvelua?

Hän on kolmannen vuoden opiskelija, joka etsi harjoittelu paikkaa. Ja löysi paikkansa WIMMA lab:sta. Ja testaaja ryhmässä Double pull.    

Haluaa keskustella projektia koskevista ongelmista. Ja pysytyä ajan tasalla mitä WIMMA lab:ssa tahpahtuu. Hän haluisi että projekteista olevat tietyt osiot näkyisivät pelkästään WIMMA lab:n jäsenille ei koko yleisölle. Ja jos kertoo jotain henkilökohtaista se ei vuotaisi koko mailmalle.   



## Arvot

* Hän arvostaa omaa yksityisyyttään ja eikä halua jakaa suurinta osaa elämästään muiden kanssa.
* On kiinnostonut analysoimaan ohjelman heikkouksia, koska nuorena kyllästy isänsä koodays sota tarinoihin. Eeppisistä koodays projekteista.
* Tietoturva hänelle hyvin tärkeä.


## Välineet ja kyvyt etc.
* Omistaa läppärin. 
* Hän suorittanut kaikki kurssit vähintään tyydyttävillä arvo sanoilla.
* On kehittynyt muutaman sovelluksen pythonilla. Kurssin ulkopuolisena aktiviteettinä.



