# Asiakas


## Viiteryhmä/segmentti:

 Team Coach


## Persoonan kuvaus

* Teemu Kontio
* WIMMA Lab Team Coach

## Motiivi käyttää/soveltaa palvelua?

Hän etsii ajankohtaista tietoa niin WimmaLabista, kuin muista IT-instituuttia koskevista asioista. Haluaa myös käyttää palvelua keskustelemiseen. Opasta WIMMA Lab ryhmäläisten ongelmien kanssa. Informoida ihmisiä mitä WIMMA Lab:ssa tapahtuu.

## Arvot

* Haluaa kehittää WIMMALabia. 
* Pitää kommunikointia kovassa arvossa.
* Ammatillinen kehitys työn kautta.


## Välineet ja kyvyt etc.
* Laaja tietämys IT-alasta.
* Rautaiset ryhmätyötaidot.
* Laadukkaat opetustaidot.




