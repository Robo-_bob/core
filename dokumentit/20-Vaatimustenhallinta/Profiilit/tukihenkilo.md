# Profiili: Moderaattori 


## Viiteryhmä/segmentti:

 
Järjestelmän tukihenkilö

## Persoonan kuvaus

* Pera Pore 
* 26v
* WIMMA lab ryhmäläinen

## Motiivi käyttää/soveltaa palvelua?

On osa WIMMA lab ryhmäläisen työvelvollisuuksia. Toimii trollejen ja bottejen tuomarina, valamiehistöna ja teloittaja virtuaalisesti. Eli pitää järjestystä yllä foorumilla ja auttaa ja neuvoo perus käyttöjiä

## Arvot

* Miksi tule tänne tekemään yleistä mielipahaa?
* Pieni pinna huonolle foorum käytökselle.
* Bänni lista on aina avoinna kaikille.

## Välineet ja kyvyt etc.
* Admin oikeudet
* Sana kielto lista. Ei bännää käyttääjää, vaan ilmoittaa moderaattorille sanasta.
* Captcha käyttöön artikkeleihin luontiin kaikille tai tietylle käyttäjälle. 

