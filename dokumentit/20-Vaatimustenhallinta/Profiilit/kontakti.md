# Profiili: Kontakti 


## Viiteryhmä/segmentti:

 WIMMA lab tuki henkilö, Alumnit


## Persoonan kuvaus

* Mika Korhonen
* Yrittäjä

## Motiivi käyttää/soveltaa palvelua?

Hän voi käyttää foorumi ohjeiden antamiseen ja kommunikointiin WIMMA lab ryhmäläisten kanssa.

## Arvot

* Tukee WIMMA lab.
* Vahvalla itsetunnolla pääsee eteenpäin.
* Itsensä mainostaminen hyvin tärkeä.


## Välineet ja kyvyt etc.
* Kokemus WIMMA lab:n töistä.
* Tietää mitä yritykset haluavat työntekijältä.
* Hyvät kommunikointi taidot.

