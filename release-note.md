# Release note

Open Project Plaform Core for Virtual Companu

Changes 29.1.2020

* Marketing and contracts folder added for finnish version
* Default Icon changed


Changes 9.9.2019

* New table of content list
* Finnish version of requirement specification updated
* MkDocks updated




Changes 3.6.2019

* Moved finish document version as baseline from ttos0800+ttos0900 opf repo 
* Removed mermaid support 
* Added support for Plant UML



Changes 27.11.2018


* Export of TTOS0800+TTOS0900 version
* Waiting for edits...
*

Known issues

* Finish versions of files..
* Just a initial version of VANILLA version
